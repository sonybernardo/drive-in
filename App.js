/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect, useMemo, useReducer } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';


import StackMenu from './app/components/navegation/StackMenu';
import { AuthContext } from './app/components/context/AuthContext';
import AsyncStorage from '@react-native-community/async-storage';

const App: () => React$Node = () => {

  const [isLoading, setLoading] = useState(true);
  const [userToken, setUserToken] = useState(null);

  const initialLoginState = {
    isLoading: true,
    userName: null,
    userToke: null,
  };

  /** Login Reducer process */
  const loginReducer = (prevState, action) => {

    if (action.type == "GET_TOKEN") {
      return {
        ...prevState,
        userToken: action.token,
        isLoading: false,
      };
    }


    if (action.type == "LOGIN") {

      return {
        ...prevState,
        userName: action.id,
        userToken: action.token,
        email: action.email,
        userId: action.phone || 0,
      };
    }


    if (action.type == "LOGOUT") {
      return {
        ...prevState,
        userToken: null,
        isLoading: false,
        userName: null,
      };
    }


    if (action.type == "REGISTER") {
      return {
        ...prevState,
        userToken: action.id,
        userName: action.token,
        isLoading: false,
      };
    }

    if (action.type == "REQUESTING_TOILET") {
      return {
        ...prevState,
        toiletRequesting: true,
      };
    }

    if (action.type == "REQUESTING_FOOD") {
      return {
        ...prevState,
        foodRequesting: true,
      };
    }

    if (action.type == "FOOD_DONE") {
      return {
        ...prevState,
        foodRequesting: false,
      };
    }

    if (action.type == "REQUEST_DONE") {
      return {
        ...prevState,
        toiletRequesting: false,
      };
    }

    if (action.type == "REQUEST_TOILET") {

      return {
        ...prevState,
        toiletRequest: action.request,
        toiletType: action.typeToilet,
        timeStamp: action.timeStamp,
      }
    }


    if (action.type == "SELECT_EVENT") {

      return {
        ...prevState,
        selectedEvent: action.data
      }

    }


    return prevState;


  };


  const [loginState, dispatch] = useReducer(loginReducer, initialLoginState)

  /** End of Login Reducer process */

  const authContext = useMemo(() => ({

    isUserLogged: () => {

      if (loginState.userToken != null) {
        return true;
      }
      return false;

    },

    getLoggedUser: () => {

      return loginState;

    },

    signIn: (username, token, phone, email) => {

      dispatch({ type: "LOGIN", id: username, token: token, phone: phone, email: email });
      return true;

    },

    logOut: async () => {
      await AsyncStorage.removeItem("nome");
      await AsyncStorage.removeItem("user");
      dispatch({ type: 'LOGOUT' })
      setLoading(false);
    },

    signUp: () => {
      setUserToken("fdsd");
      setLoading(false);
    },

    dispatch: dispatch,
    state: loginState

  }));

  useEffect(() => {

    async function processLogin() {
      const user = await AsyncStorage.getItem("user");
      const nome = await AsyncStorage.getItem("nome");
      //console.log("Ao carregar: ", user, " e ", nome);
      if (user) {
        console.log("Login com sucesso");
        authContext.signIn(nome, "n07s3t", user, '');
      }

    }

    processLogin();


  }, []);

  return (
    <AuthContext.Provider value={authContext}>
      <StackMenu />
    </AuthContext.Provider>
  )

};

const styles = StyleSheet.create({

  body: {
    backgroundColor: "white",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },

});

export default App;
