# Drive-In Fest App

### Description <br>
This is an App used when COVID was on the Rise, therefore, the application helps selling event tickets as well as manage peoples inside such event on everything they would request (e.g. meal), do (e.g. visite different areas),  and go (e.g. Toilet).

1. App Frontend <br>
This application is built in React, therefore allowing the usage of both Android and IOs, where also several features like Global Statemanagement and Push notification had to be implemented to attend specific reuqirements.

2. Backend <br>
Build in NodeJS along MongoDB, the Backend of this app follows the Microservice approach.
