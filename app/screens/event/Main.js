import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerMenu from '../../components/navegation/DrawerMenu';


const Drawer = createDrawerNavigator();

const EventHome = ({ navigation }) => {

    return (
        <DrawerMenu navigation={navigation} />
    )

}



export default EventHome;