import React, { useEffect, useState, useContext } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerMenu from '../../components/navegation/DrawerMenu';
import { MainContainer } from '../../components/container/MainContainer';
import BackButton from '../../components/container/BackButton';
import EventList from '../../components/events/EventList';
import { AuthContext } from '../../components/context/AuthContext';
import Spinner from '../../components/container/Spinner';
import { View, Text } from 'react-native';
import Icon from "react-native-vector-icons/SimpleLineIcons";
import { TouchableOpacity } from 'react-native-gesture-handler';


const Drawer = createDrawerNavigator();

const MyEvents = ({ navigation }) => {

    const [dados, setDados] = useState([]);
    const [loading, setLoading] = useState(true);
    const autContext = useContext(AuthContext);

    function getEventos() {

        setLoading(true);

        fetch(`https://driveinfest.info/backend/api/v1/events/tickets/${autContext.state.userId}`)
            .then(r => r.json())
            .then(r => {

                setDados(r);
                setLoading(false);

            })
            .catch(err => console.log("Erro: ", err));

    }

    useEffect(() => {

        //console.log(`Começou: https://driveinfest.info/backend/api/v1/events/tickets/${autContext.state.userId}`,);
        getEventos();


    }, [])

    return (
        <MainContainer>
            <BackButton navigation={navigation} />
            {
                loading
                    ?
                    <Spinner loadingText="Carregendo minha lista de eventos..." />
                    :
                    dados.length > 0
                        ?
                        <EventList title="Minha lista de eventos" userId={autContext.state.userId} actionButton={true} navigation={navigation} dados={dados} showButton={false} showStatus={true} />
                        :
                        (
                            <View style={{
                                flex: 1,
                                alignItems: "center",
                            }}>

                                <View style={{ flexDirection: "row" }}>

                                    <TouchableOpacity onPress={() => getEventos()}>
                                        <Icon name="refresh" size={20} color="white" style={{ marginRight: 20, }} />
                                    </TouchableOpacity>

                                    <Text style={{
                                        color: "white",
                                        fontSize: 17
                                    }}>Meus eventos</Text>

                                </View>

                                <Text style={{
                                    color: "white",
                                    fontSize: 20,
                                    marginTop: 170,
                                }}>Nenhum evento reservado</Text>

                            </View>
                        )
            }
        </MainContainer>
    )

}



export default MyEvents;