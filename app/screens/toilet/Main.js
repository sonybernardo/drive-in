import React, { useContext, useState, useEffect, useReducer } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { MainContainer } from '../../components/container/MainContainer';
import BackButton from '../../components/container/BackButton';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { AuthContext } from '../../components/context/AuthContext';
import HomeButton from '../../components/container/HomeButton';
import Spinner from '../../components/container/Spinner';
import AsyncStorage from '@react-native-community/async-storage';

const zones = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24
]

const ModeloOption = ({ value, onPress }) => {

    return (
        <TouchableOpacity onPress={onPress}>
            <Text style={{
                padding: 7,
                borderWidth: 1,
                marginTop: 3,
                borderRadius: 6,
                borderColor: "#e1dfdd",
                backgroundColor: "#f3f2f1"
            }}>{value}</Text>
        </TouchableOpacity>
    )

}


const ToiletReserv = ({ navigation }) => {

    const { dispatch, state } = useContext(AuthContext);
    const [loading, setLoading] = useState(false);
    const [zone, setZone] = useState(false);
    const [selectedZone, setSelectedZone] = useState(false);

    const requestToilet = (type) => {

        setLoading(true);
        const timeStamp = (new Date()).getTime();

        const dados = JSON.stringify({
            "idEvento": 0,
            "idUser": state.userId,
            "tipo": type,
            "zone": selectedZone,
            "timeStamp": timeStamp
        })

        console.log("Dados a enviar: ", dados);

        fetch("https://driveinfest.info/backend/api/v1/events/toilet/request", {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: dados
        })
            .then(r => r.json())
            .then(async r => {

                console.log("Retorno: ", r);
                dispatch({ type: 'REQUEST_TOILET', request: true, typeToilet: type, timeStamp: timeStamp.toString() });
                await AsyncStorage.setItem("requestedWc", timeStamp.toString());
                setLoading(false);
                ///console.log("Numero guardado: ", timeStamp);

            })
            .catch(err => {
                console.error("Houve um erro");
                setLoading(false);
            })

        setTimeout(() => {
            navigation.navigate('Events');
        }, 5000);
    }

    const terminarRequest = async () => {

        const timeStamp = await AsyncStorage.getItem("requestedWc");
        console.log("O numero recuperado: ", timeStamp);

        setLoading(true);
        fetch("https://driveinfest.info/backend/api/v1/events/toilet/terminate", {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify({
                "idEvento": timeStamp,
                "idUser": state.userId,
            })
        })
            .then(r => r.json())
            .then(r => {

                console.log("Terminado: ", r);
                dispatch({ type: 'REQUEST_TOILET', request: false });
                navigation.navigate('Events', { terminateRequest: true });
                setLoading(false);

                AsyncStorage.removeItem("requestedWc");

            })
            .catch(err => {

                console.log("Erro no cancelamento: ", err)
                setLoading(false);

            })


    }

    return (

        <MainContainer>
            <HomeButton navigation={navigation} />


            <View style={{ width: "100%", height: "100%", justifyContent: "center", alignItems: "center" }}>

                <View style={{ width: "90%", marginTop: -150 }}>

                    {

                        loading
                            ?
                            <Spinner loadingText="Enviando o pedido" />
                            :
                            state.toiletRequest == true
                                ?
                                <View>
                                    <View style={{ alignItems: "center", paddingBottom: 50 }}>
                                        <Text style={{ fontSize: 25, color: "white" }}>A aguardar a sua vez</Text>
                                        <Text style={{ fontSize: 13, color: "white", width: "80%", marginLeft: 6, }}>Preste atenção ao semáforo na solicitação do WC, quando estiver verde será a sua vez</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => terminarRequest()}>
                                        <View style={[styles.optionButton, styles.optionButton3]}>
                                            <Icon name="close" size={20} />
                                            <Text> Cancelar ou terminar solicitação</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                :

                                <View>

                                    <View style={{ alignItems: "center", paddingBottom: 50 }}>
                                        <Text style={{ fontSize: 20, color: "white" }}>Selecione o Genero</Text>
                                    </View>

                                    <View style={{ paddingBottom: 20, }}>

                                        <TouchableOpacity onPress={() => setZone(true)}>
                                            <View style={[styles.optionButton, styles.optionButton1, { borderRadius: 12, height: 60, }]}>
                                                <Icon name="crosshairs-gps" size={20} />
                                                <Text style={{ marginLeft: 10, }}>
                                                    {selectedZone != false ? `Zona selecionada ` : `Seleccione a Zona`}

                                                </Text>


                                                <Text>
                                                    {
                                                        selectedZone != false ?
                                                            <View
                                                                style={{
                                                                    backgroundColor: "green",
                                                                    padding: 3,
                                                                    width: 30,
                                                                    borderRadius: 100,
                                                                    marginTop: 5,
                                                                }}
                                                            >
                                                                <Text style={{ textAlign: "center", color: "white", fontSize: 10 }}>{selectedZone}</Text>
                                                            </View>

                                                            : ``
                                                    }
                                                </Text>

                                            </View>
                                        </TouchableOpacity>

                                    </View>


                                    {
                                        zone
                                            ?
                                            <ScrollView style={{
                                                position: "absolute",
                                                marginTop: 140,
                                                zIndex: 20,
                                                width: "100%",
                                                height: 150,
                                                backgroundColor: "white",
                                                borderRadius: 6,

                                            }}>

                                                < View style={{
                                                    backgroundColor: "white",
                                                    borderRadius: 6,
                                                    paddingLeft: 5,
                                                    paddingRight: 5,
                                                    paddingBottom: 6,

                                                }}>

                                                    {

                                                        zones.map((v) => <ModeloOption key={`Zona ${v}`} value={`Zona ${v}`} onPress={() => {

                                                            setSelectedZone(`${v}`);
                                                            setZone(false);

                                                        }} />)

                                                    }

                                                </ View>
                                            </ScrollView>
                                            :
                                            <View />
                                    }

                                    <View>
                                        <TouchableOpacity disabled={selectedZone != false ? false : true} onPress={() => requestToilet('MALE')}>
                                            <View style={[styles.optionButton, styles.optionButton1]}>
                                                <Icon name="human-male" size={20} color={selectedZone != false ? 'black' : 'lightgrey'} />
                                                <Text style={{ color: selectedZone != false ? 'black' : 'lightgrey' }}>Masculino</Text>
                                            </View>
                                        </TouchableOpacity>


                                        <TouchableOpacity disabled={selectedZone != false ? false : true} onPress={() => requestToilet('FEMALE')}>
                                            <View style={[styles.optionButton, styles.optionButton2]}>
                                                <Icon name="human-female" size={20} style={{ color: selectedZone != false ? 'black' : 'lightgrey' }} />
                                                <Text style={{ color: selectedZone != false ? 'black' : 'lightgrey' }}>Femenino</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                    }

                </View>

            </View>

        </MainContainer>
    );

}

const styles = StyleSheet.create({

    optionButton: {
        padding: 20,
        alignItems: "center",
        backgroundColor: "white",
        flexDirection: "row",
        justifyContent: "center"

    },

    optionButton1: {
        elevation: 3,
        shadowOffset: {
            height: 2,
            width: 3,
        },
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
    },

    optionButton2: {
        elevation: 3,
        borderBottomLeftRadius: 12,
        borderBottomRightRadius: 12,
        marginTop: 1,
    },

    optionButton3: {
        elevation: 3,
        borderRadius: 12,
        marginTop: 1,
    }

})


export default ToiletReserv;