import React, { useContext, useState, useEffect, useReducer } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { MainContainer } from '../../components/container/MainContainer';
import BackButton from '../../components/container/BackButton';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import { AuthContext } from '../../components/context/AuthContext';
import HomeButton from '../../components/container/HomeButton';
import Spinner from '../../components/container/Spinner';
import AsyncStorage from '@react-native-community/async-storage';
import SimpleIcon from "react-native-vector-icons/SimpleLineIcons";
import { ActivityIndicator } from 'react-native-paper';
const toiletIcons = {
    "FEMALE": "human-female",
    "MALE": "human-male",
}

const toiletColor = {
    "FEMALE": "pink",
    "MALE": "#23527c",
}


const AllowingButton = ({ item }) => {

    const [isAllowed, setAllowed] = useState(false);
    const [contador, setContador] = useState(0);
    const [update, setUpdating] = useState(false);
    const [unmount, setUnmount] = useState(false);

    useEffect(() => {

        if (unmount) {
            return () => {
                console.log("Componente destruído em: ", contador);
            }
        }

    }, [unmount]);

    const allowingWC = (id, timeStamp, userId) => {

        if (timeStamp) {

            const methodToCall = isAllowed == true ? "terminate" : "callrrequester";

            console.log("Methodo chamado foi: ", methodToCall);

            fetch(`https://driveinfest.info/backend/api/v1/events/toilet/${methodToCall}`, {
                method: "POST",
                headers: {
                    "content-type": "application/json"
                },
                body: JSON.stringify({
                    "idEvento": timeStamp,
                    "idUser": userId,
                })
            })
                .then(r => r.json())
                .then(r => {

                    if (methodToCall == "terminate") {
                        setUnmount(true);
                    }
                    console.log("Terminado: ", r);

                })
                .catch(err => {

                    console.log("WC - Erro no cancelamento: ", err)

                })


        }

        console.log("Concedido à: ", id);
        setAllowed(true);

        let seconds = 0;
        setInterval(() => {
            seconds++;
            setContador(seconds);
        }, 1000);

    }


    return (

        <View style={{
            backgroundColor: "white",
            paddingLeft: 13,
            padding: 11,
            borderRadius: 6,
            flexDirection: "row",
            marginTop: 10,
            display: unmount ? 'none' : 'flex',
        }}>

            <View style={{ flexDirection: "row", flex: 0.85 }}>
                <Icon
                    style={{ borderRadius: 27, padding: 1, backgroundColor: toiletColor[item.tipo] }}
                    name={toiletIcons[item.tipo]} size={20} color={"white"} />
                <Text style={{ marginLeft: 10, marginTop: 3, fontSize: 12, }}>{item.nome}</Text>
            </View>

            <TouchableOpacity onPress={() => allowingWC(item.id, item.timeStamp, item.phone)}>
                <View style={{
                    borderWidth: 1,
                    borderRadius: 6,
                    borderColor: "black",
                    alignItems: "center"
                }}>

                    {
                        !isAllowed
                            ?
                            <Icon name="play" color="green" size={25} />
                            :
                            <View style={{ alignItems: "center" }}>
                                <Icon name="stop" color="green" size={25} />
                                <Text style={{ paddingLeft: 4, paddingRight: 4, color: contador > 100 ? 'red' : 'black' }}>
                                    {contador} seg
                                </Text>
                            </View>
                    }

                </View>

            </TouchableOpacity>

        </View>

    )

}

const RequestedToilet = ({ navigation }) => {

    const { dispatch, state } = useContext(AuthContext);
    const [loading, setLoading] = useState(true);
    const [dados, setDados] = useState([]);
    const [inProgress, setProgress] = useState([]);
    const [updating, setUpdating] = useState(false);

    const requestedToilet = () => {

        fetch("https://driveinfest.info/backend/api/v1/events/toilet/list")
            .then(r => r.json())
            .then(r => {


                if (JSON.stringify(dados) != JSON.stringify(r)) {
                    setDados(r);
                }

                setLoading(false);
                setUpdating(false);

            });

    }

    useEffect(() => {

        requestedToilet();

    });

    return (

        <MainContainer>
            <HomeButton navigation={navigation} />


            <View style={{ width: "100%", height: "100%", alignItems: "center", }}>

                <View style={{ width: "90%", marginTop: 5 }}>

                    {

                        loading
                            ?
                            <Spinner loadingText="Carregando as solicitações" />
                            :
                            dados.length == 0
                                ?
                                <Text style={{
                                    textAlign: "center",
                                    marginTop: "50%",
                                    fontSize: 17,
                                    color: "white"
                                }}>Nenhum solicitação encontrada</Text>
                                :
                                <View>

                                    <View style={{ alignItems: "center", paddingBottom: 30, flexDirection: "row", alignItems: "center", justifyContent: "center" }}>

                                        <TouchableOpacity onPress={() => {
                                            setUpdating(true);
                                            requestedToilet();
                                        }}>
                                            <SimpleIcon name="refresh" size={20} color="white" />
                                        </TouchableOpacity>

                                        <Text style={{ fontSize: 20, color: "white", marginLeft: 15, }}>Solicitações ao WC</Text>

                                    </View>

                                    {
                                        updating
                                            ?
                                            <ActivityIndicator color="white" style={{ marginTop: 10, }} />
                                            :
                                            <View />
                                    }

                                    <FlatList

                                        style={{
                                            height: "85%"
                                        }}
                                        data={dados}
                                        renderItem={({ item }) => (

                                            <AllowingButton item={item} />

                                        )}

                                        keyExtractor={item => `${item.nome}-${item.timeStamp.toString()}`}

                                    />


                                </View>

                    }

                </View>

            </View>

        </MainContainer>
    );

}

const styles = StyleSheet.create({

    optionButton: {
        padding: 20,
        alignItems: "center",
        backgroundColor: "white",
        flexDirection: "row",
        justifyContent: "center"

    },

    optionButton1: {
        elevation: 3,
        shadowOffset: {
            height: 2,
            width: 3,
        },
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
    },

    optionButton2: {
        elevation: 3,
        borderBottomLeftRadius: 12,
        borderBottomRightRadius: 12,
        marginTop: 1,
    },

    optionButton3: {
        elevation: 3,
        borderRadius: 12,
        marginTop: 1,
    }

})


export default RequestedToilet;