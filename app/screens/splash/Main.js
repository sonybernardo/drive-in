import React, { useEffect } from 'react';
import { View, StyleSheet, Button, Text, Image } from 'react-native';
import { MainContainer } from '../../components/container/MainContainer';
import { ActivityIndicator } from 'react-native-paper';
import Logo from '../../components/container/Logo';


const Splash = ({ navigation, }) => {

    useEffect(() => {

        setTimeout(() => {
            navigation.navigate("Events");
        }, 3000);

    }, []);

    return (
        <MainContainer>
            <Logo>
                <ActivityIndicator size="large" color="white" />
            </Logo>
        </MainContainer>
    )

}

export default Splash;