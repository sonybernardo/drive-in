import React from 'react';
import { StyleSheet, View, Button, Image, Text } from 'react-native';
import { MainContainer } from '../../components/container/MainContainer';
import TopBar from '../../components/search/SearchBar';
import TicketsList from '../../components/ticket/TicketsList';

const MyTickets = ({navigation}) => {

    return (
        <TicketsList navigation={navigation}/>
    )

}


export default MyTickets;