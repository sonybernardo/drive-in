import React, { useState, useEffect } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerMenu from '../../components/navegation/DrawerMenu';
import { MainContainer } from '../../components/container/MainContainer';
import BackButton from '../../components/container/BackButton';
import EventList from '../../components/events/EventList';
import TopBar from '../../components/search/SearchBar';
import { View, Text } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';

const Reserve = ({ route, navigation }) => {

    //const { setStatus } = route.params;

    const [dados, setDados] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {

        var mounted = true;

        fetch("https://driveinfest.info/backend/api/v1/events", {
            method: "GET"
        })
            .then(r => r.json())
            .then(r => {

                if (mounted) {

                    let result = r.data;
                    let apiEventos = [];

                    for (let i in result) {

                        console.log("Id do evento: ", result[i].id);

                        apiEventos.push({
                            nome: result[i].attribuites.title,
                            id: parseInt(result[i].id),
                            image: result[i].attribuites.img,
                            local: result[i].attribuites.local
                        });

                    }
                    setDados(apiEventos);
                    setLoading(false);

                }
            })
            .catch(err => console.log("erro ao carregar eventos: ", err));

        return function cleanup() {
            mounted = false;
        }

    }, [])

    return (
        <MainContainer>
            <TopBar
                navigation={navigation}
                homeBotton={true}
                drawerNavigation={false} />
            <View style={{
                backgroundColor: "white",
                flex: 1,
            }}>

                {
                    loading
                        ?
                        (
                            <View style={{ flex: 1, alignItems: "center" }}>
                                <ActivityIndicator size={60} color="black" style={{ marginTop: 60 }} />
                                <Text style={{ marginTop: 5, color: "black", marginTop: 20 }}>Carregando os eventos...</Text>
                            </View>
                        )
                        :
                        (<EventList
                            title="Selecione o evento que pretende reservar"
                            titleColor="black"
                            navigation={navigation} dados={dados} />)
                }
            </View>
        </MainContainer>
    )

}



export default Reserve;