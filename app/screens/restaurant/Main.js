import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { MainContainer } from '../../components/container/MainContainer';
import BackButton from '../../components/container/BackButton';
import RestaurantList from '../../components/restaurant/RestaurantList';


const RestaurantOrder = () => {

    return (
        <MainContainer>
            <BackButton navigation={navigation} />
            <RestaurantList title="Minha lista de eventos" navigation={navigation} />
        </MainContainer>
    );

}



export default RestaurantOrder;