import React, { useState, useContext, useEffect } from 'react';
import { StyleSheet, TextInput, Button, View, Text, TouchableOpacity, Picker } from 'react-native';
import { MainContainer } from '../../components/container/MainContainer';
import BackButton from '../../components/container/BackButton';
import Logo from '../../components/container/Logo';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesom from 'react-native-vector-icons/SimpleLineIcons';
import Octicon from 'react-native-vector-icons/Octicons';
import Feather from 'react-native-vector-icons/Feather';
import { ActivityIndicator } from 'react-native-paper';
import { AuthContext } from '../../components/context/AuthContext';
import { FlatList } from 'react-native-gesture-handler';
import Spinner from '../../components/container/Spinner';
import { stat } from 'react-native-fs';
import { act } from 'react-test-renderer';

const ModeloOption = ({ value, onPress }) => {

    return (
        <TouchableOpacity onPress={onPress}>
            <Text style={{
                padding: 7,
                borderWidth: 1,
                marginTop: 3,
                borderRadius: 6,
                borderColor: "#e1dfdd",
                backgroundColor: "#f3f2f1"
            }}>{value}</Text>
        </TouchableOpacity>
    )

}

const CarItem = ({ item, display, action }) => {

    return (

        <View style={{
            backgroundColor: "#5050501a",
            padding: 5,
            borderRadius: 6,
            paddingTop: 10,
            paddingBottom: 10,
            flexDirection: "row",
            marginTop: 5,


        }}>

            <Feather name="edit" size={20} color="grey" style={{ marginLeft: 10, paddingRight: 10, display: display }} />

            <TouchableOpacity style={{}} onPress={() => {
                if (action != undefined && action != null)
                    action({ id: item.id, marca: item.marca, matricula: item.matricula });
            }}>
                <Text style={{ marginRight: 10, }}>{item.marca} - {item.matricula}</Text>
            </TouchableOpacity>
        </View>

    )

}


export const CarList = ({ dados, setView, action }) => {

    return (
        <View
            style={{
                flex: 0.48,
                width: "89%",
                marginBottom: 0,
                bottom: 0,
                paddingBottom: 30,
                borderRadius: 6,
                padding: 5,
                backgroundColor: "white",
            }}
        >

            {
                dados == null
                    ?
                    <Spinner loadingText="Carregando os veiculos" color="black" />
                    :
                    dados.length == 0
                        ?
                        <View style={{
                            flex: 1,
                            justifyContent: "center"
                        }}>

                            <Text style={{

                                textAlign: "center",
                                textAlignVertical: "center"
                            }}>Sem veiculo registado</Text>
                            <TouchableOpacity
                                style={{ width: "100%", alignItems: "center", marginTop: 30, }}
                                onPress={() => setView("ADD")}
                            >
                                <Text style={{
                                    textAlign: "center",
                                    textAlignVertical: "center",
                                    backgroundColor: "#5050501a",
                                    width: "60%",
                                    padding: 10,
                                    borderRadius: 6,
                                }}
                                >Adicionar um</Text>
                            </TouchableOpacity>

                        </View>
                        :
                        (
                            <FlatList

                                data={dados}
                                keyExtractor={(item) => item.nome}
                                renderItem={({ item }) => {

                                    if (action != null) {
                                        return (
                                            <TouchableOpacity>
                                                <CarItem item={item} key={item.id} display="none" action={action} />
                                            </TouchableOpacity>
                                        )
                                    } else {
                                        return (
                                            <CarItem item={item} key={item.id} />
                                        )
                                    }

                                }}

                            />

                        )
            }

        </View>
    )

}

export const getVeiculos = (idUser, callBack) => {

    console.log("Buscando");

    fetch(`https://driveinfest.info/backend/api/v1/carro/${idUser}`)
        .then(r => r.json())
        .then(r => {
            callBack(r);
        })
        .catch(err => console.log("Erro encontrado: ", err))

}

const Veiculo = ({ navigation }) => {

    const [modelo, setModelo] = useState(null);
    const [marca, setMarca] = useState("");
    const [cor, setCor] = useState("");
    const [matricula, setMatricula] = useState("");
    const [fieldState, setFieldStatus] = useState(true);
    const [userCreateFail, setCreateFail] = useState({ display: "none" });
    const [showModelos, setModelos] = useState(false);
    const [selectedModelo, setSelectedModelo] = useState("Modelo do carro");
    const [veiculoView, setView] = useState("LIST");
    const [meusCarros, setCarros] = useState(null);

    const { signIn, state } = useContext(AuthContext);

    useEffect(() => {

        getVeiculos(state.userId, (r) => {

            console.log("Carros encontrados: ", r)
            if (r.length == 0) {
                setCarros([]);
                return;
            }
            setCarros(r);
        })


    });

    return (
        <MainContainer>

            <BackButton navigation={navigation} />

            <View style={[styles.container, {}]}>

                <View style={{ height: "10%", marginTop: -80, width: "100%" }}>
                    {
                        fieldState
                            ?
                            (
                                <View style={{

                                    width: "88%",
                                    flexDirection: "row",
                                    marginLeft: "6%"
                                }}>

                                    <TouchableOpacity style={{ width: "50%" }} onPress={() => setView("LIST")}>

                                        <View style={[styles.optionButton, { width: "100%" }]}>

                                            <Icon
                                                color="darkred"
                                                name="car-multiple"
                                                size={20} />
                                            <Text style={{
                                                paddingLeft: 5,
                                                paddingRight: 5,
                                                color: "darkred",
                                                fontSize: 13,
                                            }}>Lista de Viaturas</Text>
                                        </View>

                                    </TouchableOpacity>



                                    <TouchableOpacity style={{ width: "50%" }} onPress={() => setView("ADD")}>
                                        <View style={[styles.optionButton, { marginLeft: "1%", width: "100%" }]}>

                                            <Icon
                                                color="darkred"
                                                name="car-arrow-right"
                                                size={20} />
                                            <Text style={{
                                                paddingLeft: 10,
                                                paddingRight: 10,
                                                color: "darkred",
                                                fontSize: 13,
                                            }}>Adicionar Viatura</Text>
                                        </View>

                                    </TouchableOpacity>


                                </View>
                            )
                            :
                            (
                                <View style={{ marginTop: 90, }}>
                                    <ActivityIndicator size="" color="white" />
                                </View>
                            )
                    }

                </View>

                <View style={[userCreateFail, [{ backgroundColor: "lightgrey", marginTop: 30, padding: 5, borderRadius: 6, width: "70%", flex: 0.29, flexDirection: "row" }]]}>
                    <Octicon name="stop" size={30} color="black" style={{ marginTop: 5, }} />
                    <Text style={{ color: "red", marginLeft: 10 }}>Este número de telefone já está registado</Text>
                </View>

                {

                    veiculoView == "LIST"
                        ?
                        (
                            <CarList dados={meusCarros} setView={setView} userId={state.userId} action={null} />
                        )
                        :
                        <FormComponent
                            setModelos={setModelos}
                            setModelo={setModelo}
                            selectedModelo={selectedModelo}
                            setSelectedModelo={setSelectedModelo}
                            setCor={setCor}
                            setMarca={setMarca}
                            setMatricula={setMatricula}
                            showModelos={showModelos}
                            fieldState={fieldState}
                            setCarros={setCarros}
                            setFieldStatus={setFieldStatus}
                            state={state}
                            setView={setView}
                        />

                }



            </View>

        </MainContainer >
    );

}

const FormComponent = (props) => {

    const [modelo, setModelo] = useState("Modelo do carro");
    const [marca, setMarca] = useState("");
    const [cor, setCor] = useState("");
    const [matricula, setMatricula] = useState("");

    function registerSuccess() {
        props.setFieldStatus(true);
        setModelo("");
        setMarca("");
        setMatricula("");
        setCor("");
    }

    function registerCarro(modelo, marca, cor, matricula) {

        props.setFieldStatus(false);

        fetch("https://driveinfest.info/backend/api/v1/carro", {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({

                "idUser": props.state.userId,
                "marca": marca,
                "cor": cor,
                "matricula": matricula,
                "modelo": modelo

            })
        })
            .then(e => e.json())
            .then(e => {

                console.log("Resultado da API: ", e);
                if (e.existing) {
                    //setCreateFail({ display: "flex" });
                    props.setFieldStatus(true);
                    return;
                }

                getVeiculos(props.state.userId, (r) => {

                    if (r.length == 0) {
                        props.setCarros([]);
                        return;
                    }

                    props.setCarros(r);
                    props.setView("LIST");
                })

                registerSuccess();

            })
            .catch(e => {
                console.log("Erro encontrado: ", e);
                props.setFieldStatus(true);
            })

    }

    return (
        <View style={{

            width: "100%"
        }}>

            <View style={[styles.inputContainer, { alignItems: "center" }]}>

                <TouchableOpacity onPress={() => {
                    props.setModelos(true);
                    setModelo(modelo);
                }}
                    style={{ width: "100%", alignItems: "center" }}>
                    <Text style={[styles.textInput, { paddingLeft: 5, paddingTop: 9 }]}>{modelo}</Text>
                </TouchableOpacity>

            </View>

            {

                props.showModelos
                    ?
                    <View style={{
                        backgroundColor: "white",
                        width: "90%",
                        borderRadius: 6,
                        paddingLeft: 5,
                        paddingRight: 5,
                        paddingBottom: 6,
                        position: "absolute",
                        zIndex: 20,
                        marginLeft: "5%"

                    }}>
                        <ModeloOption value="Ligeiro (5 pessoas)" onPress={() => {
                            setModelo("Ligeiro (5 pessoas)");
                            props.setModelos(false);
                        }} />

                        <ModeloOption value="Jeep (5 pessoas)" onPress={() => {
                            setModelo("Jeep (5 pessoas)");
                            props.setModelos(false);
                        }} />

                        <ModeloOption value="Jeep médio (8 pessoas)" onPress={() => {
                            setModelo("Jeep médio (8 pessoas)");
                            props.setModelos(false);
                        }} />

                        <ModeloOption value="Jeep grande (9 pessoas)" onPress={() => {
                            setModelo("Jeep grande (9 pessoas)");
                            props.setModelos(false);
                        }} />

                        <ModeloOption value="Carrinha Pick-ip (5 pessoas)" onPress={() => {
                            setModelo("Carrinha Pick-ip (5 pessoas)");
                            props.setModelos(false);
                        }} />


                        <ModeloOption value="x Nenhum" onPress={() => {
                            setModelo("Modelo do carro");
                            props.setModelos(false);
                        }} />

                    </View>
                    :
                    <View></View>
            }

            <View style={styles.inputContainer}>
                <TextInput style={styles.textInput} value={marca} editable={props.fieldState} onChangeText={(text) => setMarca(text)} placeholder="Marca" />
            </View>

            <View style={styles.inputContainer}>
                <TextInput style={styles.textInput} value={cor} editable={props.fieldState} onChangeText={(text) => setCor(text)} placeholder="Cor" />
            </View>

            <View style={styles.inputContainer}>
                <TextInput style={styles.textInput} value={matricula} editable={props.fieldState} onChangeText={(text) => setMatricula(text)} placeholder="Matricula" />
            </View>

            <TouchableOpacity
                onPress={() => registerCarro(modelo, marca, cor, matricula)}
                style={{ width: "100%", alignItems: "center" }}>
                <View style={styles.buttonContainer}>
                    <Icon name="content-save-move" size={25} color="white" />
                    <Text style={styles.buttonText}>CADASTRAR</Text>
                </View>
            </TouchableOpacity>
        </View>
    )

}


const styles = StyleSheet.create({

    optionButton: {

        marginTop: 10,
        flex: 0.9,
        flexDirection: "column",
        height: 30,
        alignItems: "center",
        justifyContent: "center",
        borderWidth: 1,
        width: "40%",
        paddingBottom: 30,
        paddingTop: 30,
        marginTop: -20,
        borderRadius: 6,
        borderColor: "white",
        backgroundColor: "white",

    },

    accountButtons: {
        color: "white"
    },

    container: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
    },

    textInput: {
        backgroundColor: "white",
        width: "90%",
        borderRadius: 6,
        height: 38,
    },

    inputContainer: {
        width: "100%",
        alignItems: "center",
        marginTop: 20
    },

    buttonContainer: {

        backgroundColor: "rgba(104,159,24,.5)",
        marginTop: 20,
        flexDirection: "row",
        width: "90%",
        justifyContent: "center",
        paddingVertical: 10,
        borderRadius: 3,
        alignItems: "center",
        elevation: 3,
        shadowColor: "white",
        shadowOffset: {
            width: 3
        }

    },

    buttonText: {
        color: "white",
        marginLeft: 10,
    },


});

export default Veiculo;
