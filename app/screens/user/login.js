import React, { useState, useEffect, useMemo, useContext } from 'react';
import { StyleSheet, TextInput, Button, View, Text, TouchableOpacity } from 'react-native';
import { MainContainer } from '../../components/container/MainContainer';
import BackButton from '../../components/container/BackButton';
import Logo from '../../components/container/Logo';
import Icon from 'react-native-vector-icons/Ionicons';
import { AuthContext } from '../../components/context/AuthContext';
import { DrawerContext } from '../../components/context/DrawerContext';
import AsyncStorage from '@react-native-community/async-storage';
import Reserve from '../tickets/Reserve';
import { ActivityIndicator } from 'react-native-paper';


const UserAuth = ({ navigation }) => {

    const [userName, setUserName] = useState(null);
    const [password, setPassword] = useState(null);
    const [userLoginFail, setLoginFail] = useState({ display: "none" });
    const [loggedUserToken, setUserToken] = useState(null);
    const [fieldState, setFieldStatus] = useState(true);

    const { signIn, state, dispatch } = useContext(AuthContext);
    const drawer = useContext(DrawerContext);

    const handleLogin = async () => {

        //const nomeUser = AsyncStorage.getItem("nome");
        //const userUser = AsyncStorage.getItem("user");
        //console.log(`Os dados são: ${nomeUser} e ${userUser}`);

        let errorStyle = {};
        setFieldStatus(false);

        fetch("https://driveinfest.info/backend/api/v1/user/login", {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify({
                "phone": userName,
                "password": password
            })
        })
            .then(r => r.json())
            .then(async r => {

                setFieldStatus(true);

                if (!r.success) {
                    errorStyle = {
                        marginTop: 10,
                        backgroundColor: "rgba(255,255,255,.6)",
                        width: "60%",
                        borderRadius: 6,
                        paddingLeft: 5,
                        alignItems: "center"
                    };
                    setLoginFail(errorStyle)
                    return;
                }

                await AsyncStorage.setItem("user", r.data.phone);
                await AsyncStorage.setItem("nome", r.data.nome);

                const _nomeUser = await AsyncStorage.getItem("nome");
                const _userUser = await AsyncStorage.getItem("user");

                console.log(`Depois Os dados são: ${_nomeUser} e ${_userUser}`);

                console.log("Logina realizado com user: ", r.data.email);

                signIn(r.data.nome, r.token, r.data.phone, r.data.email);

                if (state.toiletRequesting) {

                    navigation.navigate('Toilet');
                    dispatch({ type: "REQUEST_DONE" });
                    return;
                }

                navigation.navigate("Events");

                /*
                if (state.selectedEvent != null) {
                    navigation.navigate('EventDetail', state.selectedEvent);
                    return;
                }

                navigation.navigate('Reserve');
                */

            })
            .catch(err => console.log("erro: ", err));

    }

    useEffect(() => {

        AsyncStorage.getItem("userName").then(v => {
            if (v != null) {
                setUserToken(v);
                setUserName(v);
            }
        })
    })

    return (

        <MainContainer>

            <BackButton navigation={navigation} />
            <View style={styles.container}>

                <View style={{ height: "30%", marginTop: -80 }}>
                    {
                        fieldState
                            ?
                            (<Logo fontSize={20} textMarginTop={5} />)
                            :
                            (
                                <View style={{ marginTop: 90, }}>
                                    <ActivityIndicator size="" color="white" />
                                </View>
                            )
                    }
                </View>

                <View style={styles.inputContainer}>
                    <TextInput style={styles.textInput} value={userName} onChangeText={(text) => setUserName(text)} placeholder="Utilizador ou número de telefone" />
                </View>

                <View style={styles.inputContainer}>
                    <TextInput style={styles.textInput} secureTextEntry={true} onChangeText={(text) => setPassword(text)} placeholder="Senha" />
                </View>

                <View style={userLoginFail}>
                    <Text style={{ color: "red" }}>Utilizador ou senha inválda</Text>
                </View>

                <TouchableOpacity
                    onPress={() => handleLogin()} style={{ width: "100%", alignItems: "center" }}>
                    <View style={styles.buttonContainer}>
                        <Icon name="enter-outline" size={25} color="white" />
                        <Text style={styles.buttonText}>LOGAR</Text>
                    </View>
                </TouchableOpacity>

            </View>

        </MainContainer>
    );

}


const styles = StyleSheet.create({

    container: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
    },

    textInput: {
        backgroundColor: "white",
        width: "90%",
        borderRadius: 6,
        height: 38,
    },

    inputContainer: {
        width: "100%",
        alignItems: "center",
        marginTop: 20
    },

    buttonContainer: {

        backgroundColor: "rgba(104,159,24,.5)",
        marginTop: 20,
        flexDirection: "row",
        width: "90%",
        justifyContent: "center",
        paddingVertical: 10,
        borderRadius: 3,
        alignItems: "center",
        elevation: 3,
        shadowColor: "white",
        shadowOffset: {
            width: 3
        }

    },

    buttonText: {
        color: "white",
        marginLeft: 10,
    },


});

export default UserAuth;