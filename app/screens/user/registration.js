import React, { useState, useContext } from 'react';
import { StyleSheet, TextInput, Button, View, Text, TouchableOpacity } from 'react-native';
import { MainContainer } from '../../components/container/MainContainer';
import BackButton from '../../components/container/BackButton';
import Logo from '../../components/container/Logo';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesom from 'react-native-vector-icons/SimpleLineIcons';
import Octicon from 'react-native-vector-icons/Octicons';
import { ActivityIndicator, configureFonts } from 'react-native-paper';
import { AuthContext } from '../../components/context/AuthContext';


const UserRegistration = ({ navigation }) => {

    const [nome, setNome] = useState("");
    const [telefone, setTelefone] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [fieldState, setFieldStatus] = useState(true);
    const [confirm, setConfirm] = useState(true);
    const [userCreateFail, setCreateFail] = useState({ display: "none" });
    const [matchEmail, setMatch] = useState(true);
    const [submited, setSubmited] = useState(false);

    function checkEmail() {

        if (confirm === email) {
            return;
        }
        setMatch(false);

    }

    function validateForm() {

        if (nome == "" || telefone == "" || email == "" || password == "") {
            return false;
        }
        return true;

    }


    const { signIn } = useContext(AuthContext);

    function registerSuccess(token, username) {
        setFieldStatus(true);
        setNome("");
        setTelefone("");
        setEmail("");
        setPassword("");

        signIn(username, token, telefone);
        navigation.navigate("Events");

    }

    function registerUser(nome, telefone, email, password) {

        setSubmited(true);

        if (confirm !== email) {
            setMatch(false);
            return;
        }
        setMatch(true);

        if (!validateForm()) {
            console.log("Está aqui");
            return;
        }

        setMatch(true);

        setFieldStatus(false);

        fetch("https://driveinfest.info/backend/api/v1/user/register", {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                "nome": nome,
                "phone": telefone,
                "email": email,
                "tipoCarro": "Profissional",
                "password": password
            })
        })
            .then(e => e.json())
            .then(e => {

                if (e.existing) {
                    setCreateFail({ display: "flex" });
                    setFieldStatus(true);
                    return;
                }

                setCreateFail({ display: "none" });
                console.log("Resultado: ", e);
                console.log("Só o token: ", e.token);
                registerSuccess(e.token, e.username);

            })
            .catch(e => {
                console.log("Erro encontrado: ", e);
                setFieldStatus(true);
            })

    }

    return (
        <MainContainer>

            <BackButton navigation={navigation} />

            <View style={styles.container}>

                <View style={{ height: "30%", marginTop: -80 }}>
                    {
                        fieldState
                            ?
                            (<View>
                                <Logo fontSize={20} textMarginTop={5} textDisplay="none" />
                                <View style={{
                                    marginTop: 10,
                                    flex: 0.17,
                                    flexDirection: "row",
                                    height: 30,
                                    alignItems: "center",
                                    justifyContent: "center"
                                }}>


                                    <TouchableOpacity onPress={() => navigation.navigate('UserAuth')}>
                                        <View style={{
                                            flex: 1,
                                            flexDirection: "row"
                                        }}>
                                            <FontAwesom name="user-following" color="white" size={15} style={{ marginTop: 7 }} />
                                            <Text style={[styles.accountButtons, { marginLeft: 15, fontSize: 20, }]}>Já tenho conta</Text>
                                        </View>
                                    </TouchableOpacity>

                                </View>
                            </View>)
                            :
                            (
                                <View style={{ marginTop: 90, }}>
                                    <ActivityIndicator size="" color="white" />
                                </View>
                            )
                    }

                </View>

                <View style={[userCreateFail, [{ backgroundColor: "lightgrey", marginTop: 30, padding: 5, borderRadius: 6, width: "70%", flex: 0.29, flexDirection: "row" }]]}>
                    <Octicon name="stop" size={30} color="black" style={{ marginTop: 5, }} />
                    <Text style={{ color: "red", marginLeft: 10 }}>Este número de telefone já está registado</Text>
                </View>

                <View style={styles.inputContainer}>
                    <TextInput
                        style={[styles.textInput, { backgroundColor: submited && nome == "" ? 'orange' : 'white' }]}
                        value={nome} editable={fieldState} onChangeText={(text) => setNome(text)} placeholder="Nome completo" />
                </View>

                <View style={styles.inputContainer}>
                    <TextInput
                        style={[styles.textInput, { backgroundColor: submited && telefone == "" ? 'orange' : 'white' }]}
                        value={telefone} editable={fieldState} onChangeText={(text) => setTelefone(text)} placeholder="Telefone" keyboardType="numeric" />
                </View>

                <View style={styles.inputContainer}>
                    <TextInput style={[styles.textInput, { backgroundColor: matchEmail ? 'white' : 'orange' }]} value={email} keyboardType="email-address" editable={fieldState} onChangeText={(text) => setEmail(text)} placeholder="Email" />
                </View>

                <View style={styles.inputContainer}>
                    <TextInput
                        style={[styles.textInput, { backgroundColor: matchEmail ? 'white' : 'orange' }]}
                        value={confirm}
                        keyboardType="email-address"
                        editable={fieldState}
                        onBlur={() => {
                            checkEmail();
                        }}
                        onChangeText={(text) => setConfirm(text)} placeholder="Confirmar Email" />
                </View>

                <View style={styles.inputContainer}>
                    <TextInput
                        style={[styles.textInput, { backgroundColor: submited && password == "" ? 'orange' : 'white' }]}
                        value={password} secureTextEntry editable={fieldState} onChangeText={(text) => setPassword(text)} placeholder="Senha" />
                </View>

                <TouchableOpacity
                    onPress={() => registerUser(nome, telefone, email, password)}
                    style={{ width: "100%", alignItems: "center" }}>
                    <View style={styles.buttonContainer}>
                        <Icon name="content-save-move" size={25} color="white" />
                        <Text style={styles.buttonText}>CRIAR CONTA</Text>
                    </View>
                </TouchableOpacity>

            </View>

        </MainContainer>
    );

}


const styles = StyleSheet.create({

    accountButtons: {
        color: "white"
    },

    container: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
    },

    textInput: {
        backgroundColor: "white",
        width: "90%",
        borderRadius: 6,
        height: 38,
    },

    inputContainer: {
        width: "100%",
        alignItems: "center",
        marginTop: 20
    },

    buttonContainer: {

        backgroundColor: "rgba(104,159,24,.5)",
        marginTop: 20,
        flexDirection: "row",
        width: "90%",
        justifyContent: "center",
        paddingVertical: 10,
        borderRadius: 3,
        alignItems: "center",
        elevation: 3,
        shadowColor: "white",
        shadowOffset: {
            width: 3
        }

    },

    buttonText: {
        color: "white",
        marginLeft: 10,
    },


});

export default UserRegistration;