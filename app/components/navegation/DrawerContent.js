import React, { useContext, useState } from 'react';
import { View, Text, StyleSheet, Image, ImageBackground } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ion from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import SideBarButton from '../container/SideBarButton';
import { DrawerContentScrollView } from '@react-navigation/drawer';
import { AuthContext } from '../context/AuthContext';
import LogoutButton from '../container/LogoutButton';
import { TouchableOpacity } from 'react-native-gesture-handler';


const About = ({ props, setShow, showAbout }) => (

    <View>

        <DrawerHeader {...props} showAbout setShow={setShow} />

        <Text style={stylesContent.generalAboutText}>
            O Drive-in Fest é um evento de cultura, educação, filantropia,
            música, dança, teatro, cinema e artes em geral, para ser
            assistido do interior do seu veículo automóvel.
        </Text>

        <Text style={stylesContent.generalAboutText}>
            Na vertente educacional engloba desde debates de vários temas e
            entrevistas a palestras e workshops. Na vertente cultural
            teremos, música, dança, moda, artes em geral e gastronomia. No
            que toca as artes, teremos peças teatrais, sessões de cinema,
            pintura e artesanato.
                        </Text>

        <Text style={stylesContent.generalAboutText}>
            Aperte o cinto de segurança e embarque connosco numa aventura
            memorável.
                        </Text>

        <TouchableOpacity style={{ paddingBottom: 10, }} onPress={() => setShow(false)}>
            <Ion
                style={{
                    marginLeft: 10,
                    width: 34,
                    paddingLeft: 1.8,
                    marginTop: 15,
                    borderRadius: 6,
                    backgroundColor: "darkred"
                }}
                color="white"
                name="arrow-back"
                size={25} />
        </TouchableOpacity>

    </View>

)

const FAQ = ({ props, setShow, showAbout }) => (

    <View>

        <DrawerHeader {...props} showAbout setShow={setShow} />

        <View>
            <Text style={[stylesContent.generalAboutText, { fontWeight: "bold" }]}>
                Como me registo?
            </Text>
            <View>
                <Text style={[stylesContent.generalAboutText, { marginTop: -5, marginLeft: 7, width: "96%" }]}>
                    R: Pressione no menu <Text style={{ fontWeight: "bold", textDecorationLine: "underline", fontSize: 16 }}>Início</Text> na barra lateral,
                    a seguir pressione em Minha conta, caso não tenha ainda uma
                    conta digite os seus dados para registar, caso tiver já uma conta então pressione em Já tenho conta.
                </Text>
            </View>
        </View>

        <View>
            <Text style={[stylesContent.generalAboutText, { fontWeight: "bold" }]}>
                Como faço para reservar/comprar um ingresso para um dado evento?
            </Text>
            <View>
                <Text style={[stylesContent.generalAboutText, { marginTop: -5, marginLeft: 7, width: "96%" }]}>
                    R: Tendo feito o login, deverá registar primeiramente o seu/um veiculo na app atravez do
                    menu <Text style={{ fontWeight: "bold", textDecorationLine: "underline", fontSize: 16 }}>Meus veiculos</Text>
                    , tendo o veiculo sido registado, selecioner o evento na lista de eventos da tela inicial, e seguir os passos seguintes:
                    {`\n    1 - Pressionar em reservar\n    2 - Checkar a opção de concordo com os termos\n    3 - Pressional em selecionar para escolher a viatura\n    4 - pressionar em terminar e já está`}
                    .
                </Text>
            </View>
        </View>

        <View>
            <Text style={[stylesContent.generalAboutText, { fontWeight: "bold" }]}>
                Como envio o meu comprovativo de pagamento?
            </Text>
            <View>
                <Text style={[stylesContent.generalAboutText, { marginTop: -5, marginLeft: 7, width: "96%" }]}>
                    R:
                    <Text style={{ fontSize: 13 }}>
                        {`\n1- Tirar uma foto do comprovativo ou fazer uma captura de ecrã (screenshoot);`}
                        {`\n2- Pressione no menu na barra lateral e aguarde o carregamento dos ingressos reservados;`}
                        {`\n3- Pressione no botão anexar recibo de pagamento;`}
                        {`\n4- Selecione a imagem do recibo no seu telefone ou faça uma foto;`}
                        {`\n5- Selecione enviar recibo;`}
                        {`\n6- Aguarde 48h para a validação do seu recibo;`}
                        {`\n7- Se ao fim das 48h não receber o QRcode do ingresso na sua conta entre imediatamente em contacto connosco.`}
                    </Text>
                </Text>
            </View>
        </View>

        <View>
            <Text style={[stylesContent.generalAboutText, { fontWeight: "bold" }]}>
                Quanto tempo devo aguardar a confirmação do meu pagamento?
            </Text>
            <View>
                <Text style={[stylesContent.generalAboutText, { marginTop: -5, marginLeft: 7, width: "96%" }]}>
                    R: A Confirmação do pagamento dependerá do banco de origem, mas em geral levará cerca de 48 horas, após o envio do comprovativo via app.
                </Text>
            </View>
        </View>


        <TouchableOpacity style={{ paddingBottom: 10, }} onPress={() => setShow(false)}>
            <Ion
                style={{
                    marginLeft: 10,
                    width: 34,
                    paddingLeft: 1.8,
                    marginTop: 15,
                    borderRadius: 6,
                    backgroundColor: "darkred"
                }}
                color="white"
                name="arrow-back"
                size={25} />
        </TouchableOpacity>

    </View >

)

const TalkUs = ({ props, setShow, showAbout }) => (

    <View>

        <DrawerHeader {...props} showAbout setShow={setShow} />

        <Text style={stylesContent.generalAboutText}>
            Telf.: {`222 000 111   |   923 000 111`}
        </Text>
        <Text style={[stylesContent.generalAboutText, { marginTop: -5 }]}>
            Email: faleconosco@driveinfest.info
        </Text>
        <Text style={[stylesContent.generalAboutText, { marginTop: 15 }]}>
            Endereço: {`\n   Talatona, edifício Tal, 3º Andar Apto. 10`}
        </Text>


        <TouchableOpacity style={{ paddingBottom: 10, }} onPress={() => setShow(false)}>
            <Ion
                style={{
                    marginLeft: 10,
                    width: 34,
                    paddingLeft: 1.8,
                    marginTop: 15,
                    borderRadius: 6,
                    backgroundColor: "darkred"
                }}
                color="white"
                name="arrow-back"
                size={25} />
        </TouchableOpacity>

    </View >

)


const DrawerHeader = (props) => {

    const { isUserLogged, getLoggedUser } = useContext(AuthContext);

    return (
        <View style={stylesContent.contentTop}>
            <ImageBackground
                source={require("../../assets/images/app-back.png")}
                resizeMode="cover"
                style={{
                    flex: 1,
                    height: "100%",
                    width: "100%",
                }}
            >

                <View style={{
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "row",
                    marginTop: 30,
                }}>
                    <LogoutButton navigation={props.navigation} />

                    <Text
                        style={{
                            fontSize: 23,
                            color: "white",
                            marginLeft: 20,
                        }}
                    >Drive-In Fest App</Text>
                </View>

                <View style={{ marginLeft: 80, }}>
                    <Text style={{ color: "lightgrey", fontSize: 12 }}>{getLoggedUser().userName}</Text>
                </View>

            </ImageBackground>
        </View>
    )

}


const DrawerContent = (props) => {

    const { isUserLogged, getLoggedUser } = useContext(AuthContext);
    const [showAbout, setShow] = useState(false);
    const [showFAQ, setFAQ] = useState(false);
    const [showTalkUs, setTalk] = useState(false);

    return (

        <DrawerContentScrollView>

            {

                showAbout
                    ?
                    <About props={props} showAbout={showAbout} setShow={setShow} />
                    :
                    showFAQ
                        ?
                        <FAQ props={props} showAbout={showAbout} setShow={setFAQ} />
                        :
                        showTalkUs
                            ?
                            <TalkUs props={props} showAbout={showAbout} setShow={setTalk} />
                            :
                            (
                                <View>

                                    <DrawerHeader {...props} />

                                    <View style={stylesContent.wrapper}>

                                        <View style={stylesContent.container}>


                                            <SideBarButton
                                                press={() => {

                                                    setFAQ(true);
                                                    console.log("Shamou o sorbe");
                                                }}
                                                text="F.A.Q">
                                                <Icon
                                                    color="grey"
                                                    name="account-question"
                                                    size={25} />
                                            </SideBarButton>



                                            <SideBarButton
                                                press={() => props.navigation.navigate('EventsList')}
                                                text="Início">
                                                <Icon
                                                    color="grey"
                                                    name="home"
                                                    size={25} />
                                            </SideBarButton>


                                            <SideBarButton visible={!isUserLogged()}
                                                press={() => props.navigation.navigate('UserRegistration')}
                                                text="Registar-me">
                                                <Icon
                                                    color="grey"
                                                    name="car-arrow-right"
                                                    size={25} />
                                            </SideBarButton>

                                            <SideBarButton visible={isUserLogged()}
                                                press={() => props.navigation.navigate('MyAccount')}
                                                text="Minha conta">
                                                <Icon
                                                    color="grey"
                                                    name="format-list-checks"
                                                    size={25} />
                                            </SideBarButton>


                                            <SideBarButton visible={isUserLogged()}
                                                press={() => props.navigation.navigate('CarRegistration')}
                                                text="Meus Veiculos">
                                                <Icon
                                                    color="grey"
                                                    name="car-arrow-right"
                                                    size={25} />
                                            </SideBarButton>


                                            <SideBarButton visible={isUserLogged()}
                                                press={() => props.navigation.navigate('MyTickets')}
                                                text="Meus Tickets">

                                                <Fontisto
                                                    color="grey"
                                                    name="ticket-alt"
                                                    size={25} />

                                            </SideBarButton>

                                            <SideBarButton visible={isUserLogged()}
                                                press={() => { props.navigation.navigate('MyEvents') }}
                                                text="Meus eventos">
                                                <Icon
                                                    color="grey"
                                                    name="calendar-month"
                                                    size={25} />
                                            </SideBarButton>


                                            <SideBarButton
                                                press={() => {

                                                    setShow(true);
                                                    console.log("Shamou o sorbe");

                                                    /*  
                                                        setTimeout(() => {
                                                            setShow(false)
                                                        }, 5000);
                                                     */
                                                }}
                                                text="Sobre">
                                                <Icon
                                                    color="grey"
                                                    name="information-outline"
                                                    size={25} />
                                            </SideBarButton>

                                            <SideBarButton
                                                press={() => {

                                                    setTalk(true);
                                                    console.log("Shamou o sorbe");
                                                }}
                                                text="Fale conosco">
                                                <Icon
                                                    color="grey"
                                                    name="face-agent"
                                                    size={25} />
                                            </SideBarButton>

                                        </View>

                                    </View>

                                    <View>
                                        <View style={stylesContent.aboutTitle}>

                                            <Text>{/* Sobre */}</Text>
                                        </View>

                                        <View>
                                            <Text style={[stylesContent.aboutText, { fontWeight: "bold" }]}>
                                                Edição 2020. Participe já!
                                    </Text>

                                            <Text style={[stylesContent.aboutText, { marginTop: 15, fontWeight: "bold", marginLeft: -5, }]}>
                                                “Luanda Drive-in: Eco-Tech Fest”
                                    </Text>

                                            <Text style={[stylesContent.aboutText, { marginTop: 15, fontSize: 10 }]}>
                                                “Utilizar tecnologias sustentáveis é o novo modelo de desenvolvimento e garantia do futuro.”
                                    </Text>
                                        </View>

                                    </View>

                                </View>
                            )


            }
        </DrawerContentScrollView >

    )

}

const stylesContent = StyleSheet.create({

    generalAboutText: {

        width: "100%",
        padding: 3,
        paddingLeft: 10,
        marginTop: 15,
        textAlign: "justify"

    },

    contentTop: {
        backgroundColor: "red",
        height: 100,
    },

    wrapper: {
        borderColor: "lightgrey",
        borderBottomWidth: 1,
        paddingBottom: 10,
    },

    container: {
        paddingTop: 30,
        paddingLeft: 30,
    },

    aboutTitle: {
        padding: 5,
        color: "grey",
    },

    aboutText: {
        paddingLeft: 30,
        paddingRight: 20,
        textAlign: "justify",
        fontSize: 13,
    }

});



export default DrawerContent;