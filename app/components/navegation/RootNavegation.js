import React, {} from 'react';
import {
    StyleSheet,
    Image,
    Button,
    Text,
    View
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();

const RootNavigation = () => {

    return (
        <NavigationContainer>
            
            <Drawer.Navigator>

                <Drawer.Screen name="Landing" component={() => <View></View>} />                

            </Drawer.Navigator>

        </NavigationContainer>
    )

} 

export default RootNavigation;
