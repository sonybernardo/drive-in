import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import EventHome from '../../screens/event/Main';
import Splash from '../../screens/splash/Main';
import EventDetail from '../events/EventDetail';
import TicketQRCode from '../ticket/TicketQRCode';
import UserRegistration from '../../screens/user/registration';
import UserAuth from '../../screens/user/login';
import Reserve from '../../screens/tickets/Reserve';
import RestaurantList from '../restaurant/RestaurantList';
import PlacesList from '../visit/PlacesList';
import ToiletReserv from '../../screens/toilet/Main';
import Veiculo from '../../screens/user/veiculo';
import RequestedToilet from '../../screens/toilet/RequestedToilet';
import Profile from '../../screens/user/profile';



const Stack = createStackNavigator();

const StackMenu = () => {

    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Splash" screenOptions={{
                headerShown: false,
            }}>
                <Stack.Screen name="Splash" component={Splash} options={{}} />
                <Stack.Screen name="Events" component={EventHome} />
                <Stack.Screen name="EventDetail" component={EventDetail} />
                <Stack.Screen name="TicketQRCode" component={TicketQRCode} />
                <Stack.Screen name="UserRegistration" component={UserRegistration} />
                <Stack.Screen name="CarRegistration" component={Veiculo} />
                <Stack.Screen name="MyAccount" component={Profile} />
                <Stack.Screen name="UserAuth" component={UserAuth} />
                <Stack.Screen name="Reserve" component={Reserve} />
                <Stack.Screen name="OrderFood" component={RestaurantList} />
                <Stack.Screen name="Visit" component={PlacesList} />
                <Stack.Screen name="Toilet" component={ToiletReserv} />
                <Stack.Screen name="RequestedToilet" component={RequestedToilet} />
            </Stack.Navigator>
        </NavigationContainer>
    )

}

export default StackMenu;