import React, { createContext, useContext } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { View, Text, StyleSheet, Image, ImageBackground } from 'react-native';
import DrawerContent from './DrawerContent';
import EventListContent from '../events/EventListContent';
import MyTickets from '../../screens/tickets/MyTickets';
import MyEvents from '../../screens/event/MyEvents';
import {DrawerContext} from '../context/DrawerContext';
import { AuthContext } from '../context/AuthContext';

const Drawer = createDrawerNavigator();


const DrawerMenu = (obj) => {
    
    return (
        <DrawerContext.Provider value={{navigation: obj.navigation}}>

            <Drawer.Navigator
                initialRouteName="MyTickets1"
                drawerContent={props => {
                    return <DrawerContent {...props} navigation={obj.navigation} />
                }}
            >
                <Drawer.Screen name="EventsList" component={EventListContent} />
                <Drawer.Screen name="MyTickets" component={MyTickets} />
                <Drawer.Screen name="MyEvents" component={MyEvents} />

            </Drawer.Navigator>

        </DrawerContext.Provider>
    )
}


const stylesContent = StyleSheet.create({

});



export default DrawerMenu;
