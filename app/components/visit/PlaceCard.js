import React from 'react';
import { Image, View, StyleSheet, FlatList, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Octicons';
//require("../../assets/event1.jpg")

let images = [
    require("../../assets/event1.jpg"),
    require("../../assets/event2.jpg"),
    require("../../assets/event3.jpg"),
    require("../../assets/event4.jpg"),
    require("../../assets/events5.jpg"),
];

const PlaceCard = (props) => {

    const detailsContent = {
        imagem: images[parseInt(props.id)],
        id: props.id,
        nome: props.nome,
        local: props.local,
    }

    const containerStyle = {
        ...styles.container,
        backgroundColor: props.cardColor || "rgba(250,250,250,.3)",
    }

    return (



        <View style={containerStyle}>

            <View style={styles.imageContainer}>
                <Image
                    style={styles.image}
                    resizeMode="cover"
                    source={images[props.id]} />
            </View>


            <View style={styles.textContainer}>

                <View>
                    <Text style={styles.titulo}>{props.nome}</Text>
                </View>
                <TouchableOpacity>
                    <View style={styles.buttonContainer}>
                        <Icon name="tasklist" size={15} color="white" style={{ marginLeft: -10, }} />
                        <Text style={styles.local}>Agendar visita</Text>
                    </View>

                </TouchableOpacity>
            </View>

        </View>


    );

}


const styles = StyleSheet.create({

    buttonContainer: {
        backgroundColor: "grey",
        paddingHorizontal: 7,
        paddingVertical: 10,
        borderRadius: 3,
        marginTop: 10,
        width: 150,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row"
    },

    titulo: {
        fontSize: 15,
        color: "black",
        textShadowColor: "black",
    },

    local: {
        fontSize: 11,
        color: "white",
        marginLeft: 10,
    },

    container: {
        flex: 1,
        height: 100,
        backgroundColor: "rgba(250,250,250,.3)",
        width: "100%",
        marginTop: 25,
        borderRadius: 6,
        flexDirection: "row",
        padding: 6,


    },

    imageContainer: {
        backgroundColor: "white",
        width: "49%",
        borderTopLeftRadius: 6,
        borderBottomLeftRadius: 6,
    },

    image: {
        width: "100%",
        height: "100%",
        borderTopLeftRadius: 6,
        borderBottomLeftRadius: 6,
        padding: 6,
    },

    textContainer: {
        paddingLeft: 10,
    }

});


export default PlaceCard;