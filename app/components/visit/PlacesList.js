import React from 'react';
import { Image, View, StyleSheet, FlatList, Text } from 'react-native';
import { MainContainer } from '../container/MainContainer';
import TopBar from '../search/SearchBar';
import PlaceCard from './PlaceCard';
import BackButton from '../container/BackButton';

const dados = [
    { nome: "Concurso Dança", id: "1", image: "../../assets/event1.jpg", local: "Cool park" },
    { nome: "Sentada dos amigos", id: "2", image: "../../assets/event2.jpg", local: "Salão 1" },
    { nome: "Dia da criança", id: "3", image: "../../assets/event3.jpg", local: "Cinema" },
    { nome: "Stand up Comedy", id: "4", image: "../../assets/event4.jpg", local: "Parque Infantil" },
    /*{nome: "Ladyane", id: "5"},*/
];

const PlacesList = (props) => {

    const textStyle = {
        color: props.titleColor || "black",
    }

    return (

        <MainContainer>
            <BackButton navigation={props.navigation}/>
            <View style={{ ...styles.container, backgroundColor:"white" }}>
                <Text style={textStyle}>
                    {
                        (props.title != undefined) ? props.title : 'Lugares para visitar'

                    }
                </Text>
                <FlatList style={{ flex: 1, width: "90%", marginTop: 0, }}
                    data={dados} showsVerticalScrollIndicator={false}
                    renderItem={({ item }) => <PlaceCard cardColor="rgba(0,0,0,.20)" nome={item.nome} id={item.id} local={item.local} navigation={props.navigation} />}
                    keyExtractor={item => item.id}
                ></FlatList>
            </View>
        </MainContainer>


    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        width: "100%",
        marginTop: 10,

    }

})

export default PlacesList;