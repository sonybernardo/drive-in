import React from 'react';
import { StyleSheet, View, Text, Image, Button } from 'react-native';



const CaracteristicaCard = (props) => {

    return (
        <View style={styles.container}>
            
            <View>
                {props.children}
            </View>

            <Text style={styles.text}>
                {props.text}
            </Text>

        </View>
    )

}


const styles = StyleSheet.create({

    container: {

        marginHorizontal: 10,
        width: 100,
        height: 80,
        backgroundColor: "white",
        alignItems:"center",
        justifyContent: "center",
        borderRadius: 3
    },

    text:{
        borderTopWidth: 1,
        borderTopColor: "white",
        fontSize:10,

    }

});

export default CaracteristicaCard;