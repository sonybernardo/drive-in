import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, View, Text, Button, ScrollView, TouchableOpacity, Platform } from 'react-native';
import CaracteristicaCard from './Card';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconMat from 'react-native-vector-icons/MaterialIcons';
import FontIcon from 'react-native-vector-icons/FontAwesome5';
import { AuthContext } from '../context/AuthContext';
import { ToiletContext } from '../context/ToiletContext';
import { stat } from 'react-native-fs';
import PushNotification from 'react-native-push-notification';

const statusColor = {
    0: "darkorange",
    1: "green",
    2: "green",
    3: "grey",
}


const testPush = () => {

    /*
    PushNotification.localNotification({
        foreground: false,
        userInteraction: false,
        message: "My Notification Message", // (required)
        date: {},
        showWhen: true,

    });
    */

    PushNotification.localNotificationSchedule({

        message: "WC Disponível",
        date: new Date(Date.now() + 5 * 100),
        allowWhileIdle: false,

    })

}


const Caracteristicas = ({ navigation }) => {

    const autContext = useContext(AuthContext);
    const { state, isUserLogged } = autContext;
    const [toiletRequestStatus, setStatus] = useState(null);
    const [toiletRequestType, setType] = useState(null);
    const [admin, setAdmin] = useState(true);
    const [requestedToilet, setRequest] = useState(null);


    useEffect(() => {

        //console.log("O status é: ", toiletRequestStatus)


        fetch(`https://driveinfest.info/backend/api/v1/events/toilet/user/${state.userId}/${state.timeStamp}`, {
            headers: {
                "Content-type": "application/json"
            }
        })
            .then(r => r.json())
            .then(r => {

                if (r.length > 0) {
                    setRequest(r[0]);
                    if (JSON.stringify(r[0]) !== JSON.stringify(requestedToilet)) {

                        setStatus(r[0].status);
                        if (r[0].status == 2) {
                            testPush();
                            console.log("Depois: ", r);
                        }
                    }
                } else {
                    setStatus(10);
                }

            })
            .catch(err => console.log("Erro ao buscar o WC: ", err));

        setType(state.toiletType);
        console.log(requestedToilet);

    })


    return (

        <View style={styles.scrollContainer}>

            <TouchableOpacity onPress={() => testPush()}>

                <Text style={styles.title}>
                    O que pode encontrar
                </Text>

            </TouchableOpacity>

            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

                {
                    isUserLogged() == false
                        ?
                        (
                            <TouchableOpacity onPress={() => navigation.navigate('UserRegistration')}>
                                <CaracteristicaCard text="Minha conta">
                                    <Icon name="car-arrow-right" size={60} />
                                </CaracteristicaCard>
                            </TouchableOpacity>

                        )
                        :
                        <View />
                }

                {/* 
                <TouchableOpacity onPress={() => navigation.navigate('RequestedToilet')}>
                    <CaracteristicaCard text="Administração">
                        <FontIcon name="users-cog" size={50} />
                    </CaracteristicaCard>
                </TouchableOpacity>
                */}

                {/* 


                <TouchableOpacity onPress={() => navigation.navigate('RequestedToilet')}>
                    <CaracteristicaCard text="Leitura na entrada">
                        <Icon name="qrcode-scan" size={50} />
                    </CaracteristicaCard>
                </TouchableOpacity>

                */}

                <TouchableOpacity onPress={() => autContext.isUserLogged() ? navigation.navigate('Reserve') : navigation.navigate('UserAuth')}>
                    <CaracteristicaCard text="Reservar Ingresso">
                        <Icon name="ticket" size={60} />
                    </CaracteristicaCard>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {

                    if (autContext.isUserLogged())
                        navigation.navigate('Toilet');
                    else {
                        autContext.dispatch({ type: 'REQUESTING_TOILET' })
                        navigation.navigate('UserAuth');
                    }

                }}>
                    <CaracteristicaCard text="Pedir acesso ao WC">

                        <View style={{ display: (toiletRequestStatus >= 0 && toiletRequestStatus < 10) ? "flex" : "none" }}>
                            <View style={[{ ...styles.wcStatus, borderRadius: 20, backgroundColor: statusColor[toiletRequestStatus], }]}>
                                {/* <Icon name="human-male" size={20} /> */}
                            </View>
                        </View>

                        <IconMat name="wc" size={60} />
                    </CaracteristicaCard>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate('OrderFood')}>
                    <CaracteristicaCard text="Solicitar Comida">

                        <View style={{ display: (state.foodRequesting == true) ? "flex" : "none" }}>
                            <View style={[{ borderRadius: 20, }, { ...styles.wcStatus, backgroundColor: "darkorange", }]}>
                                {/* <Icon name="human-male" size={20} /> */}
                            </View>
                        </View>
                        <Icon name="food" size={60} />

                    </CaracteristicaCard>
                </TouchableOpacity>

                {/*
                <TouchableOpacity onPress={() => navigation.navigate('Visit')}>
                    <CaracteristicaCard text="Visita guiada">
                        <Icon name="panorama" size={60} />
                    </CaracteristicaCard>
                </TouchableOpacity>
                */}
            </ScrollView>

        </View >

    )

}

const styles = StyleSheet.create({

    wcStatus: {

        width: 15,
        height: 15,
        position: "absolute",
        marginLeft: -13,
        marginTop: 5,
        borderRadius: 100,

    },

    scrollContainer: {

        width: "100%",
        /*         
                borderBottomWidth: 1,
                borderBottomColor: "white",
              */
        marginTop: 30,
        height: 150,

    },

    title: {

        color: "white",
        paddingLeft: 20,
        paddingBottom: 20,
        fontSize: 18,
        paddingTop: 10,

    },

});



export default Caracteristicas;