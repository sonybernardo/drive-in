import React, { useState, useEffect, useContext } from 'react';
import { Image, View, StyleSheet, FlatList, Text } from 'react-native';
import EventCard from './RestaurantCard';
import RestaurantCard from './RestaurantCard';
import { MainContainer } from '../container/MainContainer';
import TopBar from '../search/SearchBar';
import Spinner from '../container/Spinner';
import AsyncStorage from '@react-native-community/async-storage';
import { AuthContext } from '../context/AuthContext';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const dados = [
    { nome: "Concurso Dança", id: "1", image: "../../assets/event1.jpg", local: "Cool park" },
    { nome: "Sentada dos amigos", id: "2", image: "../../assets/event2.jpg", local: "Salão 1" },
    { nome: "Dia da criança", id: "3", image: "../../assets/event3.jpg", local: "Cinema" },
    { nome: "Stand up Comedy", id: "4", image: "../../assets/event4.jpg", local: "Parque Infantil" },
    /*{nome: "Ladyane", id: "5"},*/
];


const zones = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24
]

const ModeloOption = ({ value, onPress }) => {

    return (
        <TouchableOpacity onPress={onPress}>
            <Text style={{
                padding: 7,
                borderWidth: 1,
                marginTop: 3,
                borderRadius: 6,
                borderColor: "#e1dfdd",
                backgroundColor: "#f3f2f1"
            }}>{value}</Text>
        </TouchableOpacity>
    )

}


const RestaurantList = (props) => {

    const [loading, setLoading] = useState(false);
    const [foodId, setFood] = useState(null);
    const { state, dispatch } = useContext(AuthContext);
    const [selectedZone, setSelectedZone] = useState(false);
    const [zone, setZone] = useState(false);

    const textStyle = {
        color: props.titleColor || "black",
    }

    useEffect(() => {

        AsyncStorage.getItem("requestedFoodId", (err, val) => {

            console.log("Rest seleccionado: ", val);
            setFood(val);

        });
    })

    return (

        <MainContainer>
            <TopBar backButton={true} navigation={props.navigation} />
            <View style={{ ...styles.container, backgroundColor: "white" }}>
                <Text style={textStyle}>
                    {
                        (props.title != undefined) ? props.title : 'Selecione o restaurante'

                    }
                </Text>

                {

                    zone
                        ?
                        <ScrollView style={{
                            position: "absolute",
                            marginTop: 80,
                            zIndex: 20,
                            width: "95%",
                            height: 150,
                            backgroundColor: "white",
                            borderRadius: 6,

                        }}>

                            < View style={{
                                backgroundColor: "white",
                                borderRadius: 6,
                                paddingLeft: 5,
                                paddingRight: 5,
                                paddingBottom: 6,

                            }}>

                                {

                                    zones.map((v) => <ModeloOption key={`Zona ${v}`} value={`Zona ${v}`} onPress={() => {

                                        setSelectedZone(`${v}`);
                                        setZone(false);

                                    }} />)

                                }

                            </ View>
                        </ScrollView>
                        :
                        <Text />

                }


                <View style={{ paddingBottom: 20, }}>

                    <TouchableOpacity onPress={() => {

                        if (zone)
                            setZone(false)
                        else
                            setZone(true)

                    }}>
                        <View style={[styles.optionButton, styles.optionButton1, { borderRadius: 12, height: 40, backgroundColor: "grey", flexDirection: "row", paddingTop: 10, paddingLeft: 10, paddingRight: 10, marginTop: 15, borderRadius: 6, }]}>
                            <Icon name="crosshairs-gps" size={20} color="white" />
                            <Text style={{ marginLeft: 10, color: "white" }}>
                                {selectedZone != false ? `Zona selecionada ` : `Seleccione a Zona`}

                            </Text>


                            <Text>
                                {
                                    selectedZone != false ?
                                        <View
                                            style={{
                                                backgroundColor: "green",
                                                padding: 3,
                                                width: 30,
                                                borderRadius: 100,
                                                marginTop: 5,
                                            }}
                                        >
                                            <Text style={{ textAlign: "center", color: "white", fontSize: 10 }}>{selectedZone}</Text>
                                        </View>

                                        : ``
                                }
                            </Text>

                        </View>
                    </TouchableOpacity>

                </View>

                {
                    loading
                        ?
                        <Spinner loadingText="Enviando o pedido" color="black" />
                        :
                        (
                            <FlatList style={{ flex: 1, width: "90%", marginTop: 0, }}
                                data={dados} showsVerticalScrollIndicator={false}
                                renderItem={({ item }) => <RestaurantCard selected={foodId == item.id} setLoading={setLoading} cardColor="rgba(0,0,0,.20)" nome={item.nome} id={item.id} local={item.local} navigation={props.navigation} />}
                                keyExtractor={item => item.id}
                            ></FlatList>

                        )
                }

            </View>
        </MainContainer>


    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        width: "100%",
        marginTop: 10,

    }

})

export default RestaurantList;