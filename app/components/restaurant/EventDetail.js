import React from 'react';
import { StyleSheet, Button, Image, Text, View } from 'react-native';
import { MainContainer } from '../container/MainContainer';
import Icon from "react-native-vector-icons/FontAwesome5";
import { TouchableOpacity } from 'react-native-gesture-handler';
import BackButton from '../container/BackButton';



const EventDetail = ({ route, navigation }) => {

    const { imagem, nome, local } = route.params;

    return (
        <MainContainer>

            <BackButton navigation={navigation} />

            <View style={styles.container}>

                <View style={styles.titleContainer}>
                    <Text>{nome}</Text>
                </View>

                <View style={styles.imageContainer}>
                    <Image source={imagem} resizeMode="cover" style={styles.image} />
                </View>


                <View style={styles.footerContainer}>
                    <Text>Local do evento: {local}</Text>
                    <TouchableOpacity style={{height:"85%", width:"95%", paddingBottom:10, marginTop:10, paddingHorizontal: 5, alignItems:"center"}}>
                        <View style={styles.fakeButton}>
                            <Icon name="money-check-alt" size={20} color="#666666" />
                            <Text style={styles.fakeButtonText}>Reservar Ingresso</Text>
                        </View>
                    </TouchableOpacity>
                </View>

            </View>

        </MainContainer>
    )

}

const styles = StyleSheet.create({

    fakeButtonText: {
        padding: 15,
        fontSize: 20,
        color: "#666666"
    },

    fakeButton: {
        marginTop: 5,
        height:"80%",
        width:"100%",
        marginLeft:5,
        backgroundColor: "rgba(229, 223, 223, 0.2)",
        flexDirection: "row",
        alignItems: "center",
        flex: 1,
        justifyContent: "center",
        borderRadius: 15,
        shadowColor: "black",
        shadowOffset: {
            width: 2,
            height: 5,
        },
        elevation: 3
    },


    titleContainer: {
        backgroundColor: "white",
        marginTop: 20,
        width: "90%",
        padding: 15,
        alignItems: "center",
        borderBottomColor: "black",
        borderBottomWidth: 1,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
    },


    footerContainer: {
        backgroundColor: "white",
        width: "90%",
        padding: 15,
        borderTopColor: "black",
        borderTopWidth: 1,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        height: 110,
    },


    container: {
        flex: 1,
        height: 300,
        width: "100%",
        alignItems: "center"
    },

    imageContainer: {
        width: "90%",
        height: "60%",
        marginTop: 0,
        borderRadius: 6,
        alignItems: "center",
        justifyContent: "center"
    },

    image: {
        flex: 1,
        width: "100%"
    },

    textContainer: {

    }

});


export default EventDetail;