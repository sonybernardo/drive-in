import React, { useContext } from 'react';
import { Image, View, StyleSheet, FlatList, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { AuthContext } from '../context/AuthContext';
import { stat } from 'react-native-fs';
import AsyncStorage from '@react-native-community/async-storage';
//require("../../assets/event1.jpg")

let images = [
    require("../../assets/event1.jpg"),
    require("../../assets/event2.jpg"),
    require("../../assets/event3.jpg"),
    require("../../assets/event4.jpg"),
    require("../../assets/events5.jpg"),
];

const RestaurantCard = (props) => {

    const { state, dispatch } = useContext(AuthContext);
    const { setLoading, navigation, selected } = props;

    const detailsContent = {
        imagem: images[parseInt(props.id)],
        id: props.id,
        nome: props.nome,
        local: props.local,
    }

    const containerStyle = {
        ...styles.container,
        backgroundColor: props.cardColor || "rgba(250,250,250,.3)",
    }


    const terminarRequest = async () => {

        const timeStamp = await AsyncStorage.getItem("requestedFood");
        console.log("O numero recuperado: ", timeStamp);

        setLoading(true);
        fetch("https://driveinfest.info/backend/api/v1/events/food/terminate", {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify({
                "idEvento": timeStamp,
                "idUser": state.userId,
            })
        })
            .then(r => r.json())
            .then(r => {

                console.log("Terminado: ", r);
                dispatch({ type: 'FOOD_DONE', request: false });
                navigation.navigate('Events', { terminateRequest: true });
                setLoading(false);

                AsyncStorage.removeItem("requestedFoodId");

            })
            .catch(err => {

                console.log("Erro no cancelamento: ", err)
                setLoading(false);

            })


    }

    const requestFood = (id) => {

        setLoading(true);
        const timeStamp = (new Date()).getTime();

        console.log("Estado: ", state);

        fetch("https://driveinfest.info/backend/api/v1/events/food/request", {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify({
                "idEvento": 0,
                "idUser": state.userId,
                "tipo": "FOOD",
                "timeStamp": timeStamp,
                "idRestaurante": id,
            })
        })
            .then(r => r.json())
            .then(async r => {

                console.log("Retorno: ", r);
                dispatch({ type: 'REQUESTING_FOOD' });
                await AsyncStorage.setItem("requestedFood", timeStamp.toString());
                await AsyncStorage.setItem("requestedFoodId", id);
                setLoading(false);
                console.log("Numero guardado: ", timeStamp);

            })
            .catch(err => {
                console.error("Houve um erro ao pedir comida: ", err);
                setLoading(false);
            })

        setTimeout(() => {
            navigation.navigate('Events');
        }, 5000);
    }


    return (



        <View style={containerStyle}>

            <View style={styles.imageContainer}>
                <Image
                    style={styles.image}
                    resizeMode="cover"
                    source={images[props.id]} />
            </View>


            <View style={styles.textContainer}>

                {
                    selected
                        ?
                        <View style={{ width: 18, height: 18, backgroundColor: "red", borderRadius: 12, borderWidth: 2, }}>
                        </View>
                        :
                        <View />
                }

                <View>
                    <Text style={styles.titulo}>{props.nome}</Text>
                </View>

                {
                    selected
                        ?
                        <TouchableOpacity onPress={() => terminarRequest(props.id)}>
                            <View style={[styles.buttonContainer, { backgroundColor: "orange" }]}>
                                <Icon name="stop" size={15} color="white" style={{ marginLeft: -10, }} />
                                <Text style={[styles.local,]}>Terminar Solicitação</Text>
                            </View>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity onPress={() => requestFood(props.id)}>
                            <View style={styles.buttonContainer}>
                                <Icon name="hand-right" size={15} color="white" style={{ marginLeft: -10, }} />
                                <Text style={styles.local}>Solicitar Atendente</Text>
                            </View>
                        </TouchableOpacity>

                }


            </View>

        </View>


    );

}


const styles = StyleSheet.create({

    buttonContainer: {
        backgroundColor: "darkgreen",
        paddingHorizontal: 7,
        paddingVertical: 10,
        borderRadius: 3,
        marginTop: 10,
        width: 150,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row"
    },

    titulo: {
        fontSize: 15,
        color: "black",
        textShadowColor: "black",
    },

    local: {
        fontSize: 11,
        color: "white",
        marginLeft: 10,
    },

    container: {
        flex: 1,
        height: 100,
        backgroundColor: "rgba(250,250,250,.3)",
        width: "100%",
        marginTop: 25,
        borderRadius: 6,
        flexDirection: "row",
        padding: 6,
        alignItems: "center",
        justifyContent: "center"

    },

    imageContainer: {

        width: 80,
        height: 80,
        borderTopLeftRadius: 6,
        borderBottomLeftRadius: 6,
    },

    image: {
        width: "100%",
        height: "100%",
        borderRadius: 100,
        padding: 6,
    },

    textContainer: {
        paddingLeft: 40,
    }

});


export default RestaurantCard;