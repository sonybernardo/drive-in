import React from 'react';
import { Image, View, StyleSheet, TextInput } from 'react-native';
import { Searchbar } from 'react-native-paper';
import Icon from 'react-native-vector-icons/AntDesign';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { DrawerActions } from '@react-navigation/native';
import BackButton from '../container/BackButton';
import HomeButton from '../container/HomeButton';

//const Drawer = createDrawerNavigator();
var drawerNavigation = null;


const TopBar = ({ navigation, backButton, homeBotton, drawerNavigation, searchField }) => {

    return (

        <View style={{ ...styles.container }}>
            {

                backButton == true
                    ?
                    (<View style={{ marginTop: -20, }}>
                        <BackButton navigation={navigation} />
                    </View>)
                    :
                    (<TouchableOpacity style={{ display: drawerNavigation == false ? "none" : "flex" }} onPress={() => navigation.openDrawer()}>
                        <Icon name="menu-fold" size={20} color="white" style={{ marginLeft: 10, }} />
                    </TouchableOpacity>)
            }

            {

                homeBotton
                    ?
                    (<View style={{ marginTop: -25, }}>
                        <HomeButton navigation={navigation} />
                    </View>)
                    :
                    (<View></View>)
            }

            <Searchbar placeholder="Pesquisar" style={{ borderColor: "black", width: "95%", marginLeft: 30, height: 40, display: searchField == false ? "none" : "flex" }} />
        </View>

    )

}

const styles = StyleSheet.create({
    container: {
        flex: 0.06,
        alignItems: "center",
        marginTop: 30,
        width: "80%",
        flexDirection: "row",

    }
})


export default TopBar;
