import React, { useEffect, useState } from 'react';
import { MainContainer } from '../container/MainContainer';
import TopBar from '../search/SearchBar';
import EventList from './EventList';
import Caracteristicas from '../caracteristicas/Main';
import { ActivityIndicator } from 'react-native-paper';
import { View, Text } from 'react-native';
import Spinner from '../container/Spinner';


const EventListContent = (props) => {

    const [dados, setDados] = useState([]);

    useEffect(() => {

        var mounted = true;

        fetch("https://driveinfest.info/backend/api/v1/events", {
            method: "GET"
        })
            .then(r => r.json())
            .then(r => {

                if (mounted) {

                    let result = r.data;
                    let apiEventos = [];

                    for (let i in result) {

                        console.log("Id do evento: ", result[i].id);

                        apiEventos.push({
                            nome: result[i].attribuites.title,
                            id: parseInt(result[i].id),
                            image: result[i].attribuites.img,
                            local: result[i].attribuites.local
                        });

                    }
                    setDados(apiEventos);

                }
            })
            .catch(err => console.log("erro ao carregar eventos: ", err));

        return function cleanup() {
            mounted = false;
        }

    }, [])

    return (

        <MainContainer>
            <TopBar navigation={props.navigation} />
            <Caracteristicas navigation={props.navigation} />
            {
                dados.length == 0
                    ?
                    (
                        <Spinner />
                    )
                    :
                    (<EventList navigation={props.navigation} dados={dados} />)
            }

        </MainContainer>

    );

}

export default EventListContent;