import React, { useContext, useState } from 'react';
import { Image, View, StyleSheet, FlatList, Text, TouchableOpacity } from 'react-native';
import { AuthContext } from '../context/AuthContext';

//require("../../assets/event1.jpg")


const EventCard = (props) => {

    const autContext = useContext(AuthContext);

    const detailsContent = {
        imagem: props.image,
        id: props.id,
        nome: props.nome,
        local: props.local,
        desc: props.descricao
    }

    const statusColor = {
        0: "darkorange",
        1: "darkorange",
        2: "yellow",
        3: "green",
    }

    const statusText = {
        0: "Reservado",
        1: "Reservado",
        2: "Por confirmar",
        3: "Confirmado",
    }

    const statusTextColor = {
        0: "black",
        1: "black",
        2: "black",
        3: "white",
    }



    const containerStyle = {
        ...styles.container,
        backgroundColor: props.cardColor || "rgba(250,250,250,.3)",
    }

    return (

        <TouchableOpacity onPress={() => {

            autContext.dispatch({ type: "SELECT_EVENT", data: detailsContent });
            props.navigation.navigate('EventDetail', detailsContent);
        }} disabled={props.actionButton}>

            <View style={containerStyle}>

                <View style={styles.imageContainer}>

                    <Image source={{ uri: `${props.image}` }} style={styles.image} resizeMode="cover" />

                </View>

                <View style={[styles.textContainer]}>

                    <View>
                        <Text style={styles.titulo}>{props.nome}</Text>
                    </View>

                    <View style={[styles.localContainer, { width: props.showButton ? "70%" : "100%", }]}>
                        <Text style={styles.local}>Local: {props.local}</Text>
                    </View>

                    {
                        props.showButton
                            ?
                            <View style={[styles.fakeButton, { width: "70%", marginTop: 10, flex: .7, height: "110%" }]}>
                                <Text style={[styles.fakeButtonText, { fontSize: 10, color: "white" }]}>Ver e reservar</Text>
                            </View>
                            :
                            <View />
                    }

                    {
                        props.showStatus
                            ?
                            (
                                <View style={{
                                    marginTop: 20,
                                    backgroundColor: statusColor[props.status],
                                    padding: 5,
                                    width: 100,
                                    borderRadius: 6

                                }}>
                                    <Text style={{
                                        color: statusTextColor[props.status],
                                        fontSize: 9,
                                        textAlign: "center"
                                    }}>{`${statusText[props.status]}`}</Text>
                                </View>
                            )
                            :
                            <View />
                    }

                </View>

            </View>

        </TouchableOpacity >

    );

}


const styles = StyleSheet.create({

    fakeButtonText: {
        padding: 15,
        fontSize: 20,
        color: "#666666"
    },

    fakeButton: {
        marginTop: 5,
        height: "80%",
        width: "100%",
        marginLeft: 5,
        backgroundColor: "rgba(229, 223, 223, 0.3)",
        flexDirection: "row",
        alignItems: "center",
        flex: 1,
        justifyContent: "center",
        borderRadius: 15,
        shadowColor: "black",
        shadowOffset: {
            width: 2,
            height: 5,
        },
        elevation: 3
    },

    localContainer: {
        backgroundColor: "white",
        paddingHorizontal: 7,
        borderRadius: 3,
        marginTop: 4,
    },

    titulo: {
        fontSize: 13,
        color: "white",
        textShadowColor: "black",
    },

    local: {
        fontSize: 11,
    },

    container: {
        flex: 1,
        height: 100,
        backgroundColor: "rgba(250,250,250,.3)",
        width: "100%",
        marginTop: 25,
        borderRadius: 6,
        flexDirection: "row",
        padding: 6,

    },

    imageContainer: {
        backgroundColor: "white",
        width: "50%",
        borderTopLeftRadius: 6,
        borderBottomLeftRadius: 6,
    },

    image: {
        width: "100%",
        height: "100%",
        borderTopLeftRadius: 6,
        borderBottomLeftRadius: 6,
        padding: 6,
    },

    textContainer: {
        paddingLeft: 10,
    }

});


export default EventCard;