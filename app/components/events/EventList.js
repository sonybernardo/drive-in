import React, { useState, useEffect } from 'react';
import { Image, View, StyleSheet, FlatList, Text } from 'react-native';
import EventCard from './EventCard';
import Icon from "react-native-vector-icons/SimpleLineIcons";
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ActivityIndicator } from 'react-native-paper';
/*
const dados = [
    { nome: "Concurso Dança", id: "1", image: "../../assets/event1.jpg", local: "Cool park" },
    { nome: "Sentada dos amigos", id: "2", image: "../../assets/event2.jpg", local: "Salão 1" },
    { nome: "Dia da criança", id: "3", image: "../../assets/event3.jpg", local: "Cinema" },
    { nome: "Stand up Comedy", id: "4", image: "../../assets/event4.jpg", local: "Parque Infantil" },
];
*/
const EventList = (props) => {

    const [dados, setDados] = useState(null);
    const [updating, setUpdateng] = useState(false);

    function getEvents() {

        let url = null;
        if (!props.userId) {
            url = `https://driveinfest.info/backend/api/v1/events`;
        } else {
            const userId = `/${props.userId}` || "";
            url = `https://driveinfest.info/backend/api/v1/events/tickets${userId}`;
        }

        setUpdateng(true);

        fetch(`${url}`)
            .then(r => r.json())
            .then(r => {

                if (!props.userId) {

                    let result = r.data;
                    let apiEventos = [];

                    for (let i in result) {

                        console.log("Id do evento: ", result[i].id);

                        apiEventos.push({
                            nome: result[i].attribuites.title,
                            id: parseInt(result[i].id),
                            image: result[i].attribuites.img,
                            local: result[i].attribuites.local,
                            descricao: result[i].attribuites.descricao,

                        });

                    }
                    setDados(apiEventos);

                } else {
                    setDados(r);
                }

                setUpdateng(false);

            })
            .catch(err => console.log("Erro: ", err));

    }

    useEffect(() => {

        setDados(props.dados);


        return function cleanup() {

        }

    }, [])

    const textStyle = {
        color: props.titleColor || "white",
    }

    return (

        <View style={[styles.container, {}]}>

            <View style={{ marginTop: -10, flexDirection: "row", }}>

                <TouchableOpacity onPress={() => getEvents()}>
                    <Icon name="refresh" size={20} color="white" style={{ marginRight: 20, }} />
                </TouchableOpacity>

                <Text style={textStyle}>
                    {
                        (props.title != undefined) ? props.title : 'Eventos'
                    }
                </Text>

            </View>

            {
                updating
                    ?
                    <ActivityIndicator color="white" style={{ marginTop: 10, }} />
                    :
                    <View />
            }

            <FlatList style={{ flex: 1, width: "90%", marginTop: 0, }}
                data={dados} showsVerticalScrollIndicator={false}
                renderItem={({ item }) => <EventCard showStatus={props.showStatus} actionButton={props.actionButton} showButton={(props.showButton != null && props.showButton != undefined) ? props.showButton : true} image={item.img ? item.img : item.image} cardColor="rgba(0,0,0,.20)" nome={item.nome} id={item.id} descricao={item.descricao} local={item.local} navigation={props.navigation} status={item.status} />}
                keyExtractor={item => `key-${item.id}`}
            ></FlatList>
        </View>

    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        width: "100%",
        marginTop: 10,

    }

})

export default EventList;