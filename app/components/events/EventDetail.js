import React, { useContext, useState } from 'react';
import { StyleSheet, Button, Image, Text, View, ScrollView } from 'react-native';
import { MainContainer } from '../container/MainContainer';
import Icon from "react-native-vector-icons/FontAwesome5";
import Fontiso from "react-native-vector-icons/Fontisto"
import { TouchableOpacity } from 'react-native-gesture-handler';
import BackButton from '../container/BackButton';
import { AuthContext } from '../context/AuthContext';
import Spinner from '../container/Spinner';
import QRCode from 'react-native-qrcode-svg';
import TicketQRCode from '../ticket/TicketQRCode';
import { RadioButton, Checkbox } from 'react-native-paper';
import ViewShot from 'react-native-view-shot';

import { CarList, getVeiculos } from '../../screens/user/veiculo';


const TermsConditions = ({setTerms}) => (

    <View>
        
        <ScrollView style={{
            backgroundColor: "white",
            width: "90%",
            marginLeft: "5%",
            height: "85%",
            borderRadius: 6,
            padding: 10,
        }}>

            <Text style={{ textAlign: "center", fontWeight: "bold" }}>CONCORDÂNCIA COM OS TERMOS</Text>
            <Text style={{ textAlign: "center", marginTop: 20, }}>POR FAVOR LEIA OS SEGUINTES TERMOS & CONDIÇÕES ATENTAMENTE E POR COMPLETO!</Text>

            <Text style={{
                textAlign: "justify",
                marginTop: 20,
            }}>

                Estes Termos & Condições constituem um acordo legal entre você, em nome pessoal ou como
                representante de alguma entidade e o Drive-in Fest, com relação ao seu acesso e uso do
                aplicativo e website. Você concorda que acedendo ao aplicativo, leu, compreendeu e
                concordou com os termos e condições de uso e participação do evento.
                SE VOCÊ NÃO CONCORDA COM TODOS ESTES TERMOS E CONDIÇÕES, ENTÃO ESTÁ
                EXPRESSAMENTE PROIBIDO DE USAR O APLICATIVO E PARTICIPAR NO EVENTO E DEVE
                DESINSTALÁ-LO IMEDIATAMENTE.

            </Text>

            <Text style={{
                textAlign: "justify",
                marginTop: 20,
            }}>

                Documentos adicionais sobre termos e condições podem vir a ser postados no website ou
                plataformas digitais do evento de tempos em tempos e serão incorporados neste documento
                por referências. O Drive-in Fest reserva-se no direito de, sempre que necessário, fazer
                atualizações a este Termos e Condições a qualquer momento e por qualquer razão. É da sua
                responsabilidade manter-se informado sobre as atualizações relevantes ao seu acesso ao
                aplicativo e eventos.

            </Text>


            <Text style={{ textAlign: "center", fontWeight: "bold", marginTop: 20, }}>REGISTO E CONTAS DE USUÁRIO</Text>

            <Text style={{
                textAlign: "justify",
                marginTop: 5,
            }}>

                Para o uso dos nossos serviços você deve em primeiro lugar registar-se. Ao registar-se você
                concorda em:

            </Text>

            <View>

                <Text style={{ marginLeft: 15, marginTop: 15, }}>
                    a) Fornecer os seus dados verdadeiros;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                    b) Manter os dados fornecidos ou atualizá-los imediatamente, caso ocorra alguma
                    mudança;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                    c) Que os seus dados serão ser usados para facilitar a gerência dos serviços que terá
                    acesso no evento. Se fornecer dados errados ou não atualizados, ou o Drive-in tendo
                    razões para suspeitar que os dados de usuário fornecidos não estão intencionalmente
                    corretos, o Drive-in tem o direito de suspender ou eliminar a sua conta, recusar o seu
                    uso futuro do aplicativo e barrar a sua entrada no local do evento;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                    d) Ser totalmente responsável por manter a confidencialidade da sua password. Aceita
                    a responsabilidade por toda a atividade que ocorrer na sua conta de usuário quer seja
                    através do telefone, ipad ou computador. Aconselhamos que use uma password
                    segura para se proteger contra acessos não autorizados na sua conta. Nós não
                    podemos garantir a absoluta segurança da sua conta ou da informação pessoal dada
                    e não prometemos que as nossas medidas de segurança irão impedir que terceiras pessoas (como hackers) através de meios ilegais tenham acesso a sua conta. Você
                    concorda em notificar imediatamente o Drive-in sobre qualquer uso não autorizado
                    da sua conta ou password, e confirma que entende os riscos de acesso não autorizado.
                </Text>

            </View>


            <Text style={{ textAlign: "center", fontWeight: "bold", marginTop: 20, }}>COMO FUNCIONA</Text>

            <Text style={{
                textAlign: "justify",
                marginTop: 5,
            }}>

                Os ingressos valem por veículo e não por pessoa. Com apenas 1 ingresso, você e os seus
                acompanhantes entram! No actual contexto, a lei prevê uma lotação máxima de 75% por
                veículo. Por exemplo, se o seu carro é de máxima lotação de 5 pessoas, só poderá levar consigo
                mais 3 pessoas; se o seu carro é de máxima lotação de 8 pessoas, só poderá levar consigo mais
                5 pessoas. Se você exceder este limite, a sua entrada será barrada sem devolução do valor do
                ingresso. Estarão no local quiosques de comida e bebida disponíveis para compra. Tenha o seu
                aplicativo instalado e à mão para que possa solicitar um atendente para o servir na sua viatura.
            </Text>


            <Text style={{ textAlign: "center", fontWeight: "bold", marginTop: 20, }}>REGRAS DO EVENTO</Text>

            <Text style={{
                textAlign: "justify",
                marginTop: 5,
            }}>

                Acordando com este termos e condições você concorda automaticamente com as seguintes
                regras, sob pena de que se falhar alguma delas, a sua entrada será barrada sem possibilidade
                de reembolso:
            </Text>


            <View>

                <Text style={{ marginLeft: 15, marginTop: 15, }}>
                    1. Você e os seus acompanhantes estão terminantemente proibidos de sair do carro, a
                    não ser, única e exclusivamente, para ir ao WC após receber o sinal do aplicativo que
                    chegou a sua vez;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                    2. Você deve comprar o ingresso eletrónico única e exclusivamente através do aplicativo
                    Drive-in Fest, baixar o QRcode para o seu telefone, ipad, computador ou imprimir e
                    tê-lo a mão a chegada do evento;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                    3. Você deve cadastrar o seu carro no aplicativo com dados certos e verdadeiros. Só
                    será permitida a sua entra no local se os dados do carro cadastrado corresponderem
                    ao carro que você estiver a usar;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                    4. Você receberá a chegada um livre-trânsito contendo: o número do seu veículo
                    associado a sua matrícula e o número da sua zona de estacionamento. Não é
                    permitido estacionar ou circular por outra zona que não seja a sua;
                </Text>


                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                    5. O livre-trânsito é válido para o dia todo e irá permitir-lhe entrar e sair do local do
                    evento. Sem o livre-trânsito associado a matrícula você não poderá entrar, nem sair e
                    voltar a entrar;
                </Text>


                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                    6. O Livre-trânsito deve permanecer fora do carro e num local visível a todos;
                </Text>


                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                    7. É proibido levar comida e bebida;
                </Text>


                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                    8. O seu carro e os seus ocupantes passarão por uma revista a entrada do evento;
                </Text>


                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                    9. Não há limite de idade, haverá conteúdo para toda a família.
                </Text>

            </View>


            <Text style={{ textAlign: "center", fontWeight: "bold", marginTop: 20, }}>RESERVA E PAGAMENTO DE INGRESSO</Text>

            <Text style={{
                textAlign: "justify",
                marginTop: 5,
            }}>

                Para proceder ao pagamento do ingresso deve fazer o login, registar o seu veículo através dos
                “Meus veículos”, selecione o evento na tela inicial e proceda aos passos seguintes:
                
            </Text>

            <View>

                <Text style={{ marginLeft: 15, marginTop: 15, }}>
                1- Pressionar em reservar
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                2- Checkar a opção de concordo com os termos;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                3- Selecionar a viatura;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                4- Pressionar em terminar.
                </Text>
                
            </View>

            <Text style={{
                textAlign: "justify",
                marginTop: 25,
            }}>

            Após feita a transferência bancária para o IBAN apresentado, proceda aos seguintes passos:
                
            </Text>

            <View>

                <Text style={{ marginLeft: 15, marginTop: 15, }}>
                1- Tire uma foto do comprovativo ou faça uma captura de ecrã (screenshoot);
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                2- Pressione no menu na barra lateral e aguarde o carregamento dos ingressos 
            reservados;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                3- Pressione no botão anexar recibo de pagamento;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                4- Selecione a imagem do recibo no seu telefone ou faça uma foto;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                5- Selecione enviar recibo;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                6- Aguarde 48h para a validação do seu recibo;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                7- Se ao fim das 48h não receber o QRcode do ingresso na sua conta entre 
                imediatamente em contacto connosco.
                </Text>
                
            </View>


            <Text style={{
                textAlign: "justify",
                marginTop: 25,
            }}>

                            {`Uma vez validado o pagamento do ingresso você não poderá cancelar a compra e nem 
                solicitar reembolso. 
                Você deve certificar-se de fazer a compra do ingresso pelo menos 3 dias antes da data do 
                evento a que pretende atender. Depois disso o Drive-in não se responsabiliza por quaisquer 
                falhas no sistema de validação do ingresso.`}
                
            </Text>


            <Text style={{ textAlign: "center", fontWeight: "bold", marginTop: 20, }}>TERMOS DO SERVIÇO</Text>

            <Text style={{
                textAlign: "justify",
                marginTop: 5,
            }}>

            Você expressamente reconhece e aceita que as comunicações eletrónicas estão suscetíveis
            de utilização por vários utilizadores, e como tal, sujeitas a sobrecargas informáticas, pelo que 
            o Drive-in Fest não garante a prestação do serviço sem interrupções, perda de informação ou 
            atrasos. As mensagens e ligações são respondidas apenas dentro do período normal de 
            funcionamento e por ordem de chegada. O Drive-in Fest não garante igualmente a prestação 
            do Serviço em situações de sobrecarga imprevisível dos sistemas em que o mesmo se suporta 
            ou de força maior (situações de natureza extraordinária ou imprevisível, exteriores à nós e 
            que pela mesma não possam ser controladas). O Drive-in Fest compromete-se a regularizar o 
            seu funcionamento com a maior brevidade possível. O Drive-in Fest não se responsabiliza por 
            qualquer perca, extravio, dano, atraso ou falha causados por quaisquer razões fora do nosso 
            controle.
                
            </Text>


            <Text style={{ textAlign: "center", fontWeight: "bold", marginTop: 20, }}>AVISO COVID-19</Text>

            <Text style={{
                textAlign: "justify",
                marginTop: 5,
            }}>

            O Drive-in Fest é um evento de massas sem contacto social entre os participantes. Nesta 
            senda todas as medidas de biossegurança estão acauteladas para si. Você deverá seguir todas 
            as intruções dadas pelo nosso stuff durante o evento para que tudo corra bem. Ajudemos 
            todos a manter-nos uns aos outros saudáveis!
                
            </Text>


            <Text style={{ textAlign: "center", fontWeight: "bold", marginTop: 20, }}>BIOSSEGURANÇA</Text>

            <Text style={{
                textAlign: "justify",
                marginTop: 5,
            }}>

            Todos os participantes são lembrados que a COVID-19 é uma doença extremamente 
            contagiosa que pode levar a estado grave e até a morte, portanto concordando com estes 
            termos e condições você se compromete com todos os riscos em relação a exposição ao 
            corona vírus. No caso de você ser identificado como um caso suspeito, a sua entrada no local 
            do evento será impedida e comunicaremos imediatamente às autoridades sanitárias locais.
            Você se compromete em seguir à risca as seguintes medidas de biossegurança:
                
            </Text>


            <View>

                <Text style={{ marginLeft: 15, marginTop: 15, }}>
                1- Medir temperatura a entrada do local, se estiver superior ou igual a 38 graus não será 
                permitida a sua entrada no evento;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                2- Usar a máscara facial;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                3- É terminantemente proibido sair do carro;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                4- O seu veículo não pode exceder os 75% da sua lotação máxima;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                5- Manter o distanciamento social de 2 metros;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                6- Manter o distanciamento de 2 metros entre os veículos;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                7- Obrigatório o uso correto da máscara facial e álcool em gel no WC;
                </Text>

                <Text style={{ marginLeft: 15, marginTop: 5, }}>
                8- Não ir ao WC sem aceder a fila virtual através do aplicativo.
                </Text>

            </View>


            <Text style={{ textAlign: "center", fontWeight: "bold", marginTop: 20, }}>LEI APLICÁVEL</Text>

            <Text style={{
                textAlign: "justify",
                marginTop: 5,
            }}>

            Os termos e condições regem-se pela lei angolana.
                
            </Text>


            <Text style={{ textAlign: "center", fontWeight: "bold", marginTop: 20, }}>RESOLUÇÃO DE LITÍGIOS</Text>

            <Text style={{
                textAlign: "justify",
                marginTop: 5,
            }}>

            As partes devem fazer o maior esforço para que todos os litígios, controvérsias ou 
            reivindicações sejam resolvidos amigavelmente por negociação de boa-fé. Em último caso
            serão dirimidos pelo Tribunal Provincial de Luanda, com expressa renúncia a qualquer outro 
            foro.
                
            </Text>


            <Text style={{ textAlign: "center", fontWeight: "bold", marginTop: 20, }}>AJUDA E SUPORTE</Text>

            <Text style={{
                textAlign: "justify",
                marginTop: 5,
                paddingBottom:30,
            }}>

            Iremos responder quaisquer dúvidas sobre este Termos e Condições. Para reclamações com 
            relação ao aplicativo ou para mais informação, por favor envie um e-mail 
                
            </Text>

            <TouchableOpacity style={{
                paddingBottom:40,
                alignItems:"center"
            }}
            onPress={() => setTerms(false)} 
            >
                <Text style={{
                    backgroundColor:"lightgrey",
                    height:40,
                    paddingTop:10,
                    paddingLeft:30,
                    paddingRight:30,
                    color:"black",
                    borderRadius:6,
                }}>Confirmo que li</Text>
            </TouchableOpacity>


        </ScrollView>

        
    </View >


)


const EventDetail = ({ route, navigation }) => {

    const { imagem, nome, local, id, desc } = route.params;
    const autContext = useContext(AuthContext);
    const { getLoggedUser } = useContext(AuthContext);
    const [reserving, setReserving] = useState(false);
    const [reserveSuccess, setSuccess] = useState(false);
    const [showTicketQRCode, setTicketQRCode] = useState(false);
    const [startReserv, setStartReserv] = useState(false);
    const [meusCarros, setCarros] = useState("");
    const [selectedCar, setSelectedCar] = useState(null);
    const [reservStatus, setReservStatus] = useState(false);
    const [termsChecked, setChecked] = useState(false);
    const [showInfo, setShowInfo] = useState(false);
    const [qrPDF, setQRPDF] = useState(false);
    const [showTermas, setTerms] = useState(false);


    function reservingProcess() {

        setReserving(true);
        autContext.dispatch({ type: "SELECT_EVENT", data: null });

        const qrCodeVal = selectedCar.split("\n");

        fetch("https://driveinfest.info/backend/api/v1/events/reservar", {
            body: JSON.stringify({
                "idEvento": id,
                "idUser": autContext.state.userId,
                "qrCode": `${qrCodeVal[1]}@${autContext.state.userId}@${getLoggedUser().userName}`
            }),
            method: "POST",
            headers: {
                "content-type": "application/json"
            }
        })
            .then(r => r.json())
            .then(r => {

                if (r.success) {

                    setSuccess(true);
                    setTimeout(() => {
                        setSuccess(false);
                    }, 60000);

                }

                setReserving(false);
                setTicketQRCode(true);
            })

    }

    return (
        <MainContainer>

            <BackButton navigation={navigation} />

            {
                showInfo
                    ?
                    <View style={{ width: "90%", borderRadius: 6, marginLeft: "5%", padding: 10, backgroundColor: "white", alignItems: "center" }}>
                        <View style={{ flexDirection: "row" }}>
                            <Icon name="info-circle" size={25} color="grey" />
                            <Text style={{ marginTop: 5, marginLeft: 10, }}>Informações sobre o evento</Text>
                        </View>
                        <ScrollView style={{ height: "60%" }}>
                            <Text style={{ marginLeft: 20, paddingRight: 30, marginTop: 10, textAlign: "justify", fontSize: 13, }}>{desc}</Text>
                        </ScrollView>

                        <TouchableOpacity onPress={() => setShowInfo(false)}
                            style={{ backgroundColor: "lightgrey", padding: 10, borderRadius: 6, marginTop: 4, flexDirection: "row" }}>
                            <Text>Fechar</Text>
                        </TouchableOpacity>

                    </View>
                    :
                    <View />

            }

            {
                meusCarros != ""
                    ?
                    <View style={{
                        width: "100%",
                        height: "100%",
                        alignItems: "center",
                        marginTop: 60,
                    }}>

                        <Text style={{ color: "white", paddingBottom: 20, fontSize: 16, }}>Seleccione o veiculo</Text>

                        <CarList dados={meusCarros}
                            action={({ id, marca, matricula }) => {

                                console.log("Pressionado foi o daqui: ", id);
                                setSelectedCar(`${marca}\n${matricula}`);
                                setCarros("");
                                setReservStatus(true);

                            }} />
                    </View>
                    :
                    <Text></Text>
            }

            {
                reserving
                    ?
                    (
                        <View style={{ marginTop: 130, }}>
                            <Spinner loadingText="A reservar, aguarde..." />
                        </View>
                    )
                    :
                    (
                        reserveSuccess
                            ?
                            (

                                <View style={{ marginTop: 110, flex: 1, alignItems: "center", }}>
                                    <View style={{ flex: 1, flexDirection: "column", alignItems: "center" }}>
                                        <Fontiso name="like" color="white" style={{ marginTop: 25 }} size={70} />
                                        <Text style={{ color: "white", marginTop: 20, fontSize: 25, textAlign: "center" }}>
                                            Reserva realizada com sucesso...
                                        </Text>


                                        <Text style={{ fontSize: 15, color: "white", textAlign: "center", marginTop: 20, }}>
                                            {`Coordenadas bancárias:\n Banco: BAI\nIBAN: AO06 0005 0000 0000 0000 7`}
                                        </Text>

                                        <Text style={{ fontSize: 15, color: "white", textAlign: "center", marginTop: 20, }}>
                                            (Por favor anexar o comprovativo ao seu ticket electronico para que possamos validar.)
                                        </Text>
                                    </View>
                                </View>

                            )
                            :
                            (

                                !showTicketQRCode
                                    ?
                                    showTermas
                                        ?
                                        <TermsConditions setTerms={setTerms}/>
                                        :
                                        showInfo
                                            ?
                                            <View />
                                            :
                                            <View style={styles.container}>

                                                <View style={styles.titleContainer}>
                                                    <Text>{nome}</Text>

                                                    <TouchableOpacity
                                                        onPress={() => setShowInfo(true)}
                                                        style={{ backgroundColor: "lightgrey", padding: 10, borderRadius: 6, marginTop: 4, flexDirection: "row" }}>
                                                        <Icon name="info-circle" size={17} color="grey" />
                                                        <Text style={{ fontSize: 10, marginTop: 2, marginLeft: 10, }}>Mais informações</Text>
                                                    </TouchableOpacity>

                                                </View>

                                                <View style={styles.imageContainer}>
                                                    <Image source={{ uri: `${imagem}` }} resizeMode="cover" style={styles.image} />
                                                </View>


                                                <View style={[styles.footerContainer, { alignItems: "center", height: 255 }]}>

                                                    <Text>Local do evento: {local}</Text>

                                                    {
                                                        !startReserv
                                                            ?
                                                            <TouchableOpacity
                                                                onPress={() => autContext.isUserLogged() ? setStartReserv(true) : navigation.navigate('UserAuth')}
                                                                style={{ height: 50, width: "85%", paddingBottom: 10, marginTop: 10, paddingHorizontal: 5, alignItems: "center" }}>
                                                                <View style={styles.fakeButton}>
                                                                    <Icon name="money-check-alt" size={15} color="#666666" />
                                                                    <Text style={[styles.fakeButtonText, { fontSize: 16 }]}>Reservar Ingresso</Text>
                                                                </View>
                                                            </TouchableOpacity>
                                                            :
                                                            (
                                                                <View style={{ width: "110%", alignItems: "center", marginTop: 10, borderWidth: 1, }}>



                                                                    <View style={{ flexDirection: "row" }}>

                                                                        <View style={{ flexDirection: "row" }}>
                                                                            <Checkbox status={termsChecked ? 'checked' : 'unchecked'} onPress={() => setChecked(!termsChecked)} />
                                                                            <View style={{ flexDirection: "row" }}>
                                                                                <Text style={{ marginTop: 10, fontSize: 12, }}>Concordo com os termos</Text>

                                                                                <TouchableOpacity onPress={() => setTerms(true)}>

                                                                                    <Text
                                                                                        style={{
                                                                                            marginLeft: 10,
                                                                                            marginTop: 5,
                                                                                            fontSize: 12,
                                                                                            backgroundColor: "grey",
                                                                                            height: 28,
                                                                                            paddingLeft: 6,
                                                                                            paddingRight: 6,
                                                                                            color: "white",
                                                                                            borderRadius: 6,
                                                                                            paddingTop: 5,
                                                                                        }}>Ler os termos</Text>

                                                                                </TouchableOpacity>
                                                                            </View>
                                                                        </View>

                                                                    </View>


                                                                    <RadioButton.Group>

                                                                        <View style={{ flexDirection: "row" }}>

                                                                            <Text style={{ marginTop: 8, fontSize: 14, marginLeft: -10, }}>Forma pagamento: </Text>

                                                                            <View style={{ flexDirection: "row" }}>
                                                                                <RadioButton value="Primeiro" status={"checked"} />
                                                                                <View>
                                                                                    <Text style={{ marginTop: 10, fontSize: 12, }}>Transferência Bancária</Text>
                                                                                    <Text style={{ marginTop: 0, fontSize: 9, }}>(Anexar o comprovativo ao ticket)</Text>
                                                                                </View>
                                                                            </View>

                                                                        </View>


                                                                    </RadioButton.Group>

                                                                    <View style={{ marginTop: 10, }}>
                                                                        <View style={{ flexDirection: "row" }}>
                                                                            <Text style={{ marginTop: 11, marginLeft: -35, }}>Selecionar viatura:</Text>

                                                                            <TouchableOpacity
                                                                                onPress={() => {
                                                                                    setCarros(null);
                                                                                    getVeiculos(autContext.state.userId, (r) => {

                                                                                        console.log("Veiculos encontrado: ", r);
                                                                                        setCarros(r);

                                                                                    })
                                                                                }}
                                                                                style={{ height: 50, width: 150, paddingBottom: 10, marginTop: 0, paddingHorizontal: 5, alignItems: "center" }}>
                                                                                <View style={styles.fakeButton}>
                                                                                    <Icon name="car-alt" size={15} color="#666666" style={{ marginLeft: 0, }} />
                                                                                    <Text style={[styles.fakeButtonText, { fontSize: 12 }]}>

                                                                                        {
                                                                                            selectedCar
                                                                                                ?
                                                                                                selectedCar
                                                                                                :
                                                                                                `Seleccionar`
                                                                                        }

                                                                                    </Text>
                                                                                </View>
                                                                            </TouchableOpacity>

                                                                        </View>
                                                                    </View>


                                                                    <TouchableOpacity disabled={!reservStatus && termsChecked}
                                                                        onPress={() => autContext.isUserLogged() ? reservingProcess() : navigation.navigate('UserAuth')}
                                                                        style={{ height: 50, width: "85%", paddingBottom: 10, marginTop: 10, paddingHorizontal: 5, alignItems: "center" }}>
                                                                        <View style={[styles.fakeButton, { backgroundColor: reservStatus && termsChecked ? "green" : "lightgrey" }]}>
                                                                            <Icon name="money-check-alt" size={15} color="white" style={{ marginLeft: -90, }} />
                                                                            <Text style={[styles.fakeButtonText, { fontSize: 16, color: "white" }]}>Terminar</Text>
                                                                        </View>
                                                                    </TouchableOpacity>


                                                                </View>
                                                            )

                                                    }



                                                    <View>

                                                    </View>

                                                </View>

                                            </View>
                                    :
                                    <TicketQRCode nome={nome} local={local} navigation={navigation}>
                                        <QRCode value={`${autContext.state.userId}91b3r1sh${id}`} size={220} />
                                    </TicketQRCode>

                            )
                    )
            }


        </MainContainer>
    )

}

const styles = StyleSheet.create({

    fakeButtonText: {
        padding: 15,
        fontSize: 20,
        color: "#666666"
    },

    fakeButton: {
        marginTop: 5,
        height: "80%",
        width: "100%",
        marginLeft: 5,
        backgroundColor: "rgba(229, 223, 223, 0.2)",
        flexDirection: "row",
        alignItems: "center",
        flex: 1,
        justifyContent: "center",
        borderRadius: 15,
        shadowColor: "black",
        shadowOffset: {
            width: 2,
            height: 5,
        },
        elevation: 3
    },


    titleContainer: {
        backgroundColor: "white",
        marginTop: 0,
        width: "90%",
        padding: 5,
        alignItems: "center",
        borderBottomColor: "black",
        borderBottomWidth: 1,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
    },


    footerContainer: {
        backgroundColor: "white",
        width: "90%",
        padding: 15,
        borderTopColor: "black",
        borderTopWidth: 1,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        height: 110,
    },


    container: {
        flex: 0.6,
        height: 300,
        width: "100%",
        alignItems: "center"
    },

    imageContainer: {
        width: "90%",
        height: "60%",
        marginTop: 0,
        borderRadius: 6,
        alignItems: "center",
        justifyContent: "center"
    },

    image: {
        flex: 1,
        width: "100%"
    },

    textContainer: {

    }

});


export default EventDetail;