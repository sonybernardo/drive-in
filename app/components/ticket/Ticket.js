import React, { useState, useContext, useEffect, useMemo } from 'react';
import { StyleSheet, Button, Image, Text, View } from 'react-native';
import { MainContainer } from '../container/MainContainer';
import Icon from "react-native-vector-icons/FontAwesome5";
import Ionicons from "react-native-vector-icons/Ionicons";
import { TouchableOpacity } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import { TicketContext } from '../context/TicketContext';
import { AuthContext } from '../context/AuthContext';
import ImgTo64, { stat } from 'react-native-fs';

const Tickets = ({ nome, navigation, data, local, id, status, comprovativo, idEvento, setSend }) => {

    const [receiptImage, setReceiptStatus] = useState(false);
    const [imageContent, setImageContent] = useState(null);
    const { dispatch } = useContext(TicketContext);
    const [showReceipt, setShowReceipt] = useState(false);
    const autContext = useContext(AuthContext);

    const statusColors = { 0: "darkorange", 1: "yellow", 3: "green" };

    function selectReceiptFile() {

        const options = {
            title: "Selecionar Imagem",
            storageOptions: {
                path: "images",
                skipBackup: true
            }
        }

        dispatch({ type: "SELECT_RECEIPT" });

        ImagePicker.showImagePicker(options, res => {

            //console.log(res.uri);
            if (res.uri) {
                //console.log("Tipo de ficheiro: ", `${res.data}`);
                ImgTo64.readFile(res.uri, "base64")
                    .then(r => {

                        setImageContent(`data:${res.type};base64,${res.data}`);
                        console.log("Base 64 foi carregado");
                    });

                setReceiptStatus({ uri: res.uri });
            }
            dispatch({ type: "LOADING_DONE" });

        });

    }

    //console.log(status)

    //useMemo(() => {

    //})

    function submitReceipt() {

        console.log(receiptImage);

        setSend(true);
        fetch("https://driveinfest.info/backend/api/v1/events/ticket/receipt", {
            method: "POST",
            body: JSON.stringify({
                "comprovativo": imageContent,
                "eventId": idEvento/* id */,
                "userId": autContext.state.userId,
            }),
            headers: {
                "content-type": "application/json",
            }
        })
            .then(r => r.json())
            .then(r => {
                console.log("Resultado da API: ", r);
                setSend(false);
            })

    }

    return (
        <View style={styles.container}>

            <View style={styles.titleContainer}>
                <View style={styles.eventHeader}>

                    <View style={[styles.eventHeaderSpecifi, { flexDirection: "column" }]}>

                        <View style={[styles.eventHeaderSpecifi, { width: "118%" }]}>

                            <View style={[
                                { width: 15, height: 15, borderRadius: 20, marginTop: 4, marginLeft: -5, },
                                { backgroundColor: statusColors[status], borderWidth: status == 1 ? 2 : 0, borderRadius: 20, }
                            ]} />
                            <Text style={[styles.eventTitle, { marginLeft: 5, width: "100%", }]}>
                                {nome}
                            </Text>

                        </View>

                        <View style={{ height: receiptImage == false && !showReceipt ? 0 : 150, marginTop: 10, }}>

                            {
                                showReceipt
                                    ?
                                    (
                                        <Image source={{ uri: comprovativo }} resizeMode="cover" style={{ width: "100%", height: "100%", }} />
                                    )
                                    :
                                    receiptImage != false
                                        ?
                                        <Image source={receiptImage} resizeMode="cover" style={{ width: "100%", height: "100%", }} />
                                        :
                                        (<View></View>)
                            }

                        </View>


                        {
                            status == 1
                                ?
                                (
                                    <View style={{
                                        marginTop: -30,
                                        paddingLeft: 3,
                                        borderRadius: 3,
                                        backgroundColor: "yellow",
                                        borderWidth: 1.2,
                                    }}>
                                        <Text style={{
                                            fontSize: 12,
                                            color: "black",
                                        }}>
                                            Comprovativo submetido, aguardando a confirmação
                                        </Text>
                                    </View>

                                )
                                :
                                status == 3
                                    ?
                                    (
                                        <View style={{
                                            marginTop: -30,
                                            paddingLeft: 3,
                                            borderRadius: 3,
                                            backgroundColor: "darkgreen",
                                            padding: 5,
                                        }}>
                                            <Text style={{
                                                fontSize: 12,
                                                color: "white",
                                            }}>
                                                Pagamento confirmado
                                        </Text>
                                        </View>

                                    )
                                    :
                                    (
                                        <View style={{
                                            marginTop: -30,
                                            paddingLeft: 3,
                                            borderRadius: 3,
                                            backgroundColor: "darkorange"
                                        }}>
                                            <Text style={{
                                                fontSize: 12,
                                                color: "white",
                                            }}>
                                                Reserva de ingresso efectuada
                                        </Text>
                                        </View>

                                    )

                        }

                    </View>


                    <View style={{ height: "75%", }}>

                        <TouchableOpacity
                            style={{ height: 35, width: 140, }}
                            onPress={() => navigation.navigate("TicketQRCode", { existing: true, nome: nome, local: local, id: id, status: status })}>

                            <View style={[styles.showQrButton, { marginLeft: 30 }]}>
                                <Icon name="qrcode" size={16} />
                                <Text style={[styles.showQrButtonText, { fontSize: 10, width: "60%" }]}>Ver Ingresso (QRCode)</Text>
                            </View>
                        </TouchableOpacity>
                        {

                            status >= 1
                                ?
                                (
                                    <View>
                                        <TouchableOpacity
                                            style={{ height: 35, width: 140, marginTop: 12, }}
                                            onPress={() => {
                                                if (showReceipt == true) {
                                                    setShowReceipt(false);
                                                    return;
                                                }
                                                setShowReceipt(true);
                                            }}>

                                            <View style={[styles.showQrButton, { marginLeft: 30 }]}>
                                                <Ionicons name="receipt" color="darkorange" size={16} style={{ elevation: 2, }} />
                                                <Text style={[styles.showQrButtonText, { fontSize: 10, width: "60%" }]}>Exibir recibo</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                )
                                :
                                receiptImage != false
                                    ?
                                    (
                                        <View>
                                            <TouchableOpacity
                                                style={{ height: 35, width: 140, marginTop: 12, }}
                                                onPress={() => submitReceipt()}>

                                                <View style={[styles.showQrButton, { marginLeft: 30 }]}>
                                                    <Ionicons name="send" color="green" size={16} />
                                                    <Text style={[styles.showQrButtonText, { fontSize: 10, width: "60%" }]}>Enviar Recibo</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                style={{ height: 35, width: 140, marginTop: 12, }}
                                                onPress={() => setReceiptStatus(false)}>

                                                <View style={[styles.showQrButton, { marginLeft: 30 }]}>
                                                    <Ionicons name="ios-remove-circle-sharp" color="red" size={16} />
                                                    <Text style={[styles.showQrButtonText, { fontSize: 10, width: "60%" }]}>Ocultar/Excluir Recibo</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    )
                                    :
                                    (
                                        <TouchableOpacity
                                            style={{ height: 35, width: 140, marginTop: 12, }}
                                            onPress={() => selectReceiptFile()}>

                                            <View style={[styles.showQrButton, { marginLeft: 30 }]}>
                                                <Ionicons name="receipt" size={16} />
                                                <Text style={[styles.showQrButtonText, { fontSize: 10, width: "60%" }]}>Anexar Recibo de Pagamento</Text>
                                            </View>
                                        </TouchableOpacity>
                                    )

                        }


                    </View>

                </View>
            </View>

            <View style={styles.footerContainer}>
                <View>
                    <Text style={{}}>{local} | Data: {data}</Text>
                </View>
            </View>


        </View >
    )

}

const styles = StyleSheet.create({

    eventHeaderSpecifi: {
        width: "70%",
        flexDirection: "row",
        flex: 1,
    },

    eventTitle: {
        fontWeight: "bold",
        fontSize: 16,
        color: "grey",
        width: "55%"
    },

    eventHeader: {
        flexDirection: "row",
        alignItems: "center"
    },

    showQrButtonText: {
        fontSize: 11,
        marginLeft: 10,
    },

    showQrButton: {
        flexDirection: "row",
        marginTop: 3,
        marginLeft: 10,
        flex: 1,
        justifyContent: "center",
        backgroundColor: "lightgrey",
        alignItems: "center",
        borderRadius: 6,
        height: 40,
        width: 110,

    },

    titleContainer: {
        backgroundColor: "white",
        marginTop: 15,
        width: "90%",
        padding: 15,
        paddingTop: 5,
        borderBottomColor: "lightgrey",
        borderBottomWidth: 1,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
    },


    footerContainer: {
        backgroundColor: "white",
        width: "90%",
        padding: 15,
        paddingTop: 0,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        height: 30,
    },


    container: {

        width: "100%",
        alignItems: "center",
        marginTop: 0,
        marginBottom: 15,
    },

    imageContainer: {
        width: "90%",
        height: "100%",
        marginTop: 0,
        borderRadius: 6,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "white",
        borderColor: "black",
    },

    image: {
        flex: 1,
        width: "100%",
        height: "100%",
        resizeMode: "cover"
    },

});


export default Tickets;