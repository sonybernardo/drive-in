import React, { useEffect, useContext, useState, useReducer, useMemo } from 'react';
import { StyleSheet, Button, Image, Text, View, FlatList } from 'react-native';
import { MainContainer } from '../container/MainContainer';
import Icon from "react-native-vector-icons/FontAwesome5";
import { TouchableOpacity } from 'react-native-gesture-handler';
import Ticket from '../../components/ticket/Ticket';
import BackButton from '../container/BackButton';
import { AuthContext } from '../context/AuthContext';
import Spinner from '../container/Spinner';
import { TicketContext } from '../context/TicketContext';
import { ActivityIndicator } from 'react-native-paper';
import SimpleIcon from "react-native-vector-icons/SimpleLineIcons";
import { useSafeArea } from 'react-native-safe-area-context';
import { call } from 'react-native-reanimated';

function reducer(prevState, action) {


    if (action.type == "SELECT_RECEIPT") {
        return {
            ...prevState,
            loading: true,
        }
    }

    if (action.type == "LOADING_DONE") {
        return {
            ...prevState,
            loading: false,
        }
    }

    return prevState;

}


const TicketsList = ({ navigation }) => {

    const autContext = useContext(AuthContext);
    const [dados, setDados] = useState([]);
    const [loading, setLoading] = useState(true);
    const [sendLoading, setSend] = useState(false);
    const [updating, setUpdating] = useState(false);

    const [state, dispatch] = useReducer(reducer, { loading: false });

    function getTickets(callBack, update) {

        if (update != false) setUpdating(true);

        fetch(`https://driveinfest.info/backend/api/v1/events/tickets/${autContext.state.userId}`)
            .then(r => r.json())
            .then(r => {

                callBack(r);

            });

    }


    useEffect(() => {

        getTickets((r) => {
            setDados(r);
            setLoading(false);
        }, false);
        //console.log("Valor do estado: ", state);

    }, []);

    const ticketContext = useMemo(() => ({

        dispatch: dispatch,
        state: state,

    }));

    const { imagem, nome, local } = {};

    return (
        <MainContainer>

            <TicketContext.Provider value={ticketContext}>

                {
                    state.loading == true
                        ?
                        <View style={[styles.modalCourtain, { height: '100%' }]} >
                            <ActivityIndicator color="white" size="100" />
                            <Text style={{ fontSize: 20, color: "white", marginTop: 20, }}>Processando...</Text>
                        </View>
                        :
                        <View />

                }


                <BackButton navigation={navigation} />

                {
                    loading
                        ?
                        (
                            <View style={{ marginTop: 150, }}>
                                <Spinner loadingText="Carregando os meus tickets..." />
                            </View>
                        )
                        :
                        dados.length > 0
                            ?
                            sendLoading
                                ?
                                <View style={{ marginTop: 150, }}>
                                    <Spinner loadingText="Enviando o comprovativo, aguarde..." />
                                </View>
                                :
                                <View>

                                    <View style={{ paddingBottom: 10, flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                                        <TouchableOpacity onPress={() => {

                                            getTickets((r) => {
                                                setDados(r);
                                                setUpdating(false);
                                            }, true)

                                        }}>
                                            <SimpleIcon name="refresh" size={20} color="white" />
                                        </TouchableOpacity>

                                        <Text style={{ textAlign: "center", color: "white", fontSize: 16, marginLeft: 15 }}>Lista de Tickets</Text>
                                    </View>

                                    {
                                        updating
                                            ?
                                            <ActivityIndicator color="white" style={{ marginTop: 10, }} />
                                            :
                                            <View />
                                    }

                                    <FlatList
                                        style={{ marginTop: -3 }}
                                        data={dados}
                                        keyExtractor={item => `ticket-${item.id}`}
                                        renderItem={({ item }) => <Ticket key={`ticket-${item.id}`} {...item} setSend={setSend} navigation={navigation} />}
                                    />

                                </View>

                            :
                            (
                                <View style={{
                                    flex: .5,
                                    alignItems: "center",
                                    justifyContent: "center"
                                }}>
                                    <View style={{ marginTop: -70, flexDirection: "row", width: "100%", alignItems: "center", justifyContent: "center" }}>
                                        <TouchableOpacity onPress={() => {

                                            getTickets((r) => {
                                                setDados(r);
                                                setUpdating(false);
                                            }, true)

                                        }}>
                                            <SimpleIcon name="refresh" size={20} color="white" />
                                        </TouchableOpacity>

                                        <Text style={{
                                            color: "white",
                                            fontSize: 17,
                                            paddingLeft: 10,
                                        }}>Meus Tickets</Text>

                                    </View>

                                    {
                                        updating
                                            ?
                                            <View>
                                                <ActivityIndicator color="white" style={{ marginTop: 10, }} />
                                            </View>
                                            :
                                            <View />
                                    }

                                    <Text style={{
                                        color: "white",
                                        fontSize: 20,
                                        marginTop: 130,
                                    }}>Nenhum ticket encontrado</Text>

                                </View>
                            )

                }


            </TicketContext.Provider>
        </MainContainer>
    )

}

const styles = StyleSheet.create({

    modalCourtain: {

        position: "absolute",
        height: "100%",
        width: "100%",
        backgroundColor: "black",
        zIndex: 1,
        opacity: .8,
        flex: 1,
        justifyContent: "center",
        alignItems: "center"

    },

});


export default TicketsList;