import React, { useContext, useRef } from 'react';
import { StyleSheet, Button, Image, Text, View, PermissionsAndroid } from 'react-native';
import { MainContainer } from '../container/MainContainer';
import Icon from "react-native-vector-icons/FontAwesome5";
import { TouchableOpacity } from 'react-native-gesture-handler';
import BackButton from '../container/BackButton';
import QRCode from 'react-native-qrcode-svg';
import { AuthContext } from '../context/AuthContext';
import ViewShot, { captureScreen } from 'react-native-view-shot';

//import RNFD from 'react-native-file-download';


const image = require("../../assets/images/qr.png");


const pdfGenerate = async (imageUri) => {



}

const TicketQRCode = ({ route, navigation, nome, local, children, id }) => {

    const { state } = useContext(AuthContext);
    const viewShot = useRef(null);

    let existingQrCode = false;
    if (route) {
        existingQrCode = route.params.existing ? true : false;
        nome = route.params.nome;
        local = route.params.local;
    }

    return (
        <MainContainer>

            <View style={{ flex: 0.05, alignItems: "center", marginTop: 30, }}>
                <Text style={{ color: "white", fontSize: 25, }}>O Seu ingresso abaixo</Text>
            </View>

            <View style={[styles.container]}>

                <View style={styles.titleContainer}>
                    <Text>{nome}</Text>
                </View>

                <View style={styles.imageContainer}>

                    {
                        existingQrCode
                            ?
                            <QRCode value={`${state.userId}91b3r1sh${id}`} size={220} />
                            :
                            children
                    }



                </View>


                <View style={styles.footerContainer}>

                    <Text>Local do evento: {local}</Text>
                    {/* 

                    <TouchableOpacity
                        onPress={() => {

                            captureScreen({
                                format: 'jpg',
                                quality: 0.9,
                            }).then(uri => {

                                pdfGenerate(uri);
                                console.log("Image URI: ", uri);


                            })

                        }}
                        style={{ height: 60, width: "95%", paddingBottom: 10, marginTop: 5, paddingHorizontal: 5, alignItems: "center" }}>
                        <View style={styles.fakeButton}>
                            <Icon name="file-download" size={20} color="#666666" />
                            <Text style={styles.fakeButtonText}>Baixar</Text>
                        </View>
                    </TouchableOpacity>

 */}
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ height: 60, width: "95%", paddingBottom: 10, marginTop: 0, paddingHorizontal: 5, alignItems: "center" }}>
                        <View style={styles.fakeButton}>
                            <Icon name="money-check-alt" size={20} color="#666666" />
                            <Text style={styles.fakeButtonText}>Voltar</Text>
                        </View>
                    </TouchableOpacity>

                    <ViewShot ref={viewShot} options={{ format: "jpg", quality: 0.9 }}></ViewShot>

                </View>

            </View>

        </MainContainer>
    )

}

const styles = StyleSheet.create({

    fakeButtonText: {
        padding: 15,
        fontSize: 20,
        color: "#666666"
    },

    fakeButton: {
        marginTop: 5,
        height: "80%",
        width: "100%",
        marginLeft: 5,
        backgroundColor: "rgba(229, 223, 223, 0.2)",
        flexDirection: "row",
        alignItems: "center",
        flex: 1,
        justifyContent: "center",
        borderRadius: 15,
        shadowColor: "black",
        shadowOffset: {
            width: 2,
            height: 5,
        },
        elevation: 3
    },


    titleContainer: {
        backgroundColor: "white",
        marginTop: 20,
        width: "90%",
        padding: 15,
        alignItems: "center",
        borderBottomColor: "black",
        borderBottomWidth: 1,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
    },


    footerContainer: {
        backgroundColor: "white",
        width: "90%",
        padding: 15,
        borderTopColor: "black",
        borderTopWidth: 1,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        height: 170,
    },


    container: {
        flex: 1,
        height: 250,
        width: "100%",
        alignItems: "center",
        marginTop: 20,
    },

    imageContainer: {
        width: "90%",
        height: "50%",
        marginTop: 0,
        borderRadius: 6,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "white"
    },

    image: {
        flex: 1,
        width: "100%"
    },

    textContainer: {

    }

});


export default TicketQRCode;