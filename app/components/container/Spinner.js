import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';


const Spinner = (props) => {

    const text = props.loadingText || "Carregando os dados...";
    const color = props.color || "white";


    return (
        <View style={{ flex: 1, alignItems: "center" }}>
            <ActivityIndicator size={60} color={color} style={{ marginTop: 60 }} />
            <Text style={{ marginTop: 5, color: color, marginTop: 35, }}>{text}</Text>
        </View>
    )


}


export default Spinner;