import React from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';



const SideBarButton = (props) => {

    let displayStatus = {
        display: (props.visible != undefined) && (props.visible == false) ? "none" : "flex",
    }

    return (
        <TouchableOpacity onPress={props.press} style={displayStatus}>
            <View
                style={styles.buttonContainer}>
                {props.children}
                <Text style={styles.buttonText}>{props.text}</Text>
            </View>
        </TouchableOpacity>
    )


}

const styles = StyleSheet.create({

    buttonText: {
        paddingLeft: 10,
        color: "grey",
    },

    container: {
        paddingTop: 5,
        paddingLeft: 30,
    },

    buttonContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 21,
    }

});

export default SideBarButton;



