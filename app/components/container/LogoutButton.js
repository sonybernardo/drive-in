import React, { useContext } from 'react';
import Icon from 'react-native-vector-icons/AntDesign';
import { View, Text } from 'react-native';
import { AuthContext } from '../context/AuthContext';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

const LogoutButton = ({ navigation }) => {

    const { logOut, isUserLogged } = useContext(AuthContext);
    const processLogout = () => {
        logOut();
        AsyncStorage.removeItem("userName");
        //navigation.closeDrawer();
    }

    let displayStatus = {
        display: !isUserLogged() ? "none" : "flex",
        backgroundColor: "white",
        padding: 10,
        borderRadius: 6,
    }

    return (
        <View>
            <TouchableOpacity
                style={{
                    alignItems: "center"
                }}
                onPress={() => processLogout()}>
                <Icon
                    name="logout"
                    color="darkred"
                    size={15}
                    style={[displayStatus]}
                />
                <Text style={{ color: "white" }}>sair</Text>
            </TouchableOpacity>
        </View>
    )

}


export default LogoutButton;