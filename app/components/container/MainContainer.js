import React from 'react';
import { Image, View, StyleSheet, Text, ImageBackground, StatusBar } from 'react-native';


export const MainContainer = function (props) {

    return (
        <View style={{ flex: 1, width: "100%", height: "100%", alignItems: "center", justifyContent: "center" }}>
            <StatusBar translucent backgroundColor="transparent" />
            <ImageBackground
                style={styles.container}
                source={require("../../assets/images/app-back.png")}>

                {props.children}

            </ImageBackground>
        </View>
    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: "100%",
        width: "100%",
    }
})
