import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

const Logo = (props) => {

    let componentStyle = { ...styles.logoText };
    if (props.fontSize)
        componentStyle.fontSize = props.fontSize;

    if (props.textMarginTop)
        componentStyle.marginTop = props.textMarginTop;

    return (
        <View style={styles.home}>
            <Image source={require("../../assets/logo_w.png")}
                style={{ width: 200, height: 180, resizeMode: "cover", borderRadius: 12, paddingBottom: 20 }}
                resizeMode="cover" />
            <Text style={[componentStyle, { display: props.textDisplay || "flex" }]}>
                Drive-In F3st App
                </Text>

            {props.children}

        </View>
    );

}


const styles = StyleSheet.create({

    logoText: {
        color: "white",
        fontSize: 22,
        marginTop: 30
    },

    home: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },

    background: {
        flex: 1,
        width: "100%",
        height: "100%"
    }

})

export default Logo;