import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';




const BackButton = ({ navigation }) => {

    return (
        <TouchableOpacity onPress={() => navigation.goBack()}>
            <View style={styles.buttonContainer}>
                <Icon
                    style={styles.iconStyle}
                    name="return-up-back"
                    size={25} color="white" />
                    {/* <Text style={styles.buttonText}>Voltar</Text> */}
            </View>
        </TouchableOpacity>
    )

}

const styles = StyleSheet.create({

    buttonText:{
        color:"white",
        marginTop:20,
        marginLeft:10,
    },

    buttonContainer: {
        flexDirection:"row", 
        alignItems:"center",
    },

    iconStyle: {
        marginLeft: 10,
        marginTop: 20,
    }

});

export default BackButton;